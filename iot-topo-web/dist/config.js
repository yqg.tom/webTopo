//config.js文件
//组态数据主题：IOT/WEBTOPO/DATA/地址的guid
//组态数据:{"A1":10},A1为点位管理中的变量标识
window.env = {
    //组态图库图片请求前缀
    VUE_APP_IMAGE_URL:"http://localhost:11061",
    //EMQX消息服务器配置
    VUE_APP_EMQX_SERVER_URL:"ws://81.68.197.219:8083/mqtt",
    VUE_APP_EMQX_USERNAME:"emqx",
    VUE_APP_EMQX_PASSWORD:"public",
    //组态编辑器跳转地址
    VUE_APP_BASE_URL:"http://localhost:11062/webTopo",
}