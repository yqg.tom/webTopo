## 开发

云组态体验地址：https://www.yuque.com/longlong-jtoqy/wzqn3q/webtopo

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

初始账号密码：admin admin123

组态编辑器为已打包的项目dist，体验组态编辑器请按照以下部署体验，源码请联系作者，谢谢。

eqmx的websocket需要配置iot-topo-web/dist/config.js

//组态数据主题：IOT/WEBTOPO/DATA/地址的guid
//组态数据:{"A1":10},A1为点位管理中的变量标识

window.env = {
    //组态图库图片请求前缀
    VUE_APP_IMAGE_URL:"http://localhost:11061",
    //EMQX消息服务器配置
    VUE_APP_EMQX_SERVER_URL:"ws://81.68.197.219:8083/mqtt",
    VUE_APP_EMQX_USERNAME:"emqx",
    VUE_APP_EMQX_PASSWORD:"public",
    //组态编辑器跳转地址
    VUE_APP_BASE_URL:"http://localhost:11062/webTopo",
}

组态编辑器的nginx配置需要/webTopo、/webTopo/prod-api/ 

nginx的配置



       server {
       listen 11060;
       server_name  localhost;
      	# 开启解压缩静态文件
      	gzip_static on;
        location / {
            root   P:\BT\WebTopo\dist;
            index  index.html index.htm;
            try_files  $uri $uri/ /index.html;
        }
        location /webTopo {
            alias  P:\BT\WebTopo\webTopo\dist;
            try_files $uri $uri/ /webTopo/index.html;
            index  index.html;
        }
        location /webTopo/prod-api/ {
          proxy_set_header Host $http_host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header REMOTE-HOST $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_pass http://localhost:11061/;    # 后端访问接口
        }
        location /prod-api/ {
          proxy_set_header Host $http_host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header REMOTE-HOST $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_pass http://localhost:11061/;    # 后端访问接口
     }
}
QQ：扫一扫添加
![QQ](1731912093925.jpg)
微信：扫一扫添加
![微信](1731912138455.jpg)
软件证书:
![软著](1731912182239.jpg)