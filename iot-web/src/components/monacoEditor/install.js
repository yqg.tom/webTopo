
import MonacoEditor from '@/components/monacoEditor'
 
export default {
  install: function (Vue, options) {
    // 引号中的字符串就是之后的组件标签名，即 <MonacoEditor></MonacoEditor>
    Vue.component('MonacoEditor', MonacoEditor)
  }
}
