const tipUtil = {};

//死区设置说明
tipUtil.getSqsz = function() {
  return '<h3><span style="color: rgb(255, 153, 0);">什么是死区</span></h3><p>死区是将平台采集到的变量值，重要的变化保留，非重要的变化过滤或者忽略的一种机制。变量设置死区后，当变量本次采集到的数值与上次采集到的数值之间的差值小于死区值时，平台实时监控中显示的变量值依旧为上次采集到的数值；反之，平台实时监控中显示的变量值为本次最新采集到的数值。</p><p><br></p><h3><span style="color: rgb(255, 153, 0);">应用举例</span></h3><p>变量A显示的数值为0.1，设置的死区为0.3，当最新一次采集到的数值为0.2，因为|0.2-0.1|=0.1&lt;0.3,即差值小于死区，变量A依旧显示0.1；当最新一次采集到的数值为0.6，因为|0.6-0.1|=0.5&gt;0.3,即差值大于死区，变量A更新显示0.6。</p><p><br></p><h3><span style="color: rgb(255, 153, 0);">如何设置合适的死区值</span></h3><p>死区可设置的范围是：0.00000001~1000，建议将变量数值量程的1%左右值作为死区的值，您可以根据您的实际情况进行配置，合理的死区范围，能够帮助您的网关节省更多流量。</p><p><br></p><h3><span style="color: rgb(255, 153, 0);">死区会对什么功能有影响</span></h3><p>1.变量实时监控功能，设置合理的死区，能够过滤变量不必要的数值，显示必要的数值</p><p>2.在历史报表，变化存储模式和条件存储模式中，对应的波动范围值和条件值的范围需要大于死区值，否则历史报表则会按照死区范围进行存储。</p>'
};
//网关说明
tipUtil.getGatewayDescription=function(){
  return '<h2><sup style="color: rgb(255, 153, 0);">CPE系列设备新特性</sup></h2><p>1.CPE系列设备支持市面多种品牌PLC驱动，无需对数据进行modbus类型转换，配置完毕后即可进行查看数据监控，简单方便。</p><p><br></p><p>2.单台网关可连接多台PLC，可使用RS232、RS485、LAN端口进行数据通讯（部分型号有一定区别)。</p><p><br></p><p>3.支持云组态，权限管控，快速复制，多模式实时监控和多种报表存储，满足多样化数据监控需求。</p><p><br></p><h2><span style="color: rgb(255, 153, 0);">如何添加CPE系列设备</span></h2><p>1.完善设备基本信息，可填写设备名称，设备位置和相关备注信息等。</p><p><br></p><p>2.设置网关、驱动，填写设备标签中IMEI号，即可绑定CPE网关，CPE网关支持多个设备驱动，可通过RS232、RS485、LAN等端口与PLC进行数据通讯。</p><p><br></p><p>3.添加变量配置，可进行批量导入，复制，排序等操作，可设置多条件报警。</p><p><br></p>'
}
//分享说明
tipUtil.getShareDescription=function(){
  return '<h2><sup style="color: rgb(255, 153, 0);">分享特性</sup></h2><p>生成组态预览的二维码，用户无需登录系统，直接微信或者浏览器扫描二维码即可浏览设备组态。</p><p><br></p>'
}
//获取当前时间
tipUtil.getNowTime=function(){
    //年
  let year = new Date().getFullYear();
  //月份是从0月开始获取的，所以要+1;
  let month = new Date().getMonth() +1;
  if(month<10){
    month='0'+month;
  }
  //日
  let day = new Date().getDate();
  if(day<10){
    day='0'+day;
  }
  //时
  let hour = new Date().getHours();
  if(hour<10){
    hour='0'+hour;
  }
  //分
  let minute = new Date().getMinutes();
  if(minute<10){
    minute='0'+minute;
  }
  //秒
  let second = new Date().getSeconds();
  if(second<10){
    second='0'+second;
  }
  return year + '-' + month + '-' + day+" "+hour+":"+minute+":"+second;
}
//获取当前时间小时
tipUtil.getFrontTime=function(val){
var frontOneHour = new Date(new Date().getTime() - val * 60 * 60 * 1000);
  //年
let year = frontOneHour.getFullYear();
//月份是从0月开始获取的，所以要+1;
let month = frontOneHour.getMonth() +1;
//日
let day = frontOneHour.getDate();
//时
let hour = frontOneHour.getHours();
//分
let minute = frontOneHour.getMinutes();
//秒
let second = frontOneHour.getSeconds();
return year + '-' + month + '-' + day+" "+hour+":"+minute+":"+second;
}
//协议转换公式
tipUtil.getConvertDescription=function(){
  return `<p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">{</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"store"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:{</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"book"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:[</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"category"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:</span><strong style="color: rgb(58, 181, 74); background-color: rgb(255, 255, 255);">"reference"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">,</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"price"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:</span><strong style="color: rgb(37, 170, 226); background-color: rgb(255, 255, 255);">8.95</strong></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;],</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"bicycle"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:{</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"color"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:</span><strong style="color: rgb(58, 181, 74); background-color: rgb(255, 255, 255);">"red"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">,</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong style="color: rgb(146, 39, 143); background-color: rgb(255, 255, 255);">"price"</strong><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">:</span><strong style="color: rgb(37, 170, 226); background-color: rgb(255, 255, 255);">19.95</strong></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;}</span></p><p><span style="color: rgb(74, 85, 96); background-color: rgb(255, 255, 255);">}</span></p><p><strong style="color: rgb(33, 37, 41); background-color: rgb(248, 249, 250);">例子:</strong></p><p><span style="background-color: rgb(248, 249, 250);">获得第一本书的类别</span><span style="background-color: rgb(248, 249, 250); color: rgb(33, 37, 41);">：</span><span style="background-color: rgb(240, 240, 240); color: rgb(230, 0, 0);">store.book[0].category；</span></p><p><span style="background-color: rgb(240, 240, 240);">获得第一本书的价格：</span><span style="background-color: rgb(240, 240, 240); color: rgb(230, 0, 0);">store.book[1].</span><span style="color: rgb(230, 0, 0);">price；</span></p><p><span style="background-color: rgb(240, 240, 240);">获取自行车的价格：</span><span style="background-color: rgb(240, 240, 240); color: rgb(230, 0, 0);">store.</span><span style="color: rgb(230, 0, 0);">bicycle</span><span style="color: rgb(230, 0, 0); background-color: rgb(240, 240, 240);">.</span><span style="color: rgb(230, 0, 0);">price</span><span style="color: rgb(230, 0, 0); background-color: rgb(240, 240, 240);">；</span></p>`;
}
//公式说明=
tipUtil.getFormulaDescription=function(){
  return '<h2><span style="color: rgb(255, 153, 0);">设备变量公式使用说明</span></h2><p class="ql-align-justify">1、<strong>采集公式</strong>功能是为了将网关采集到的变量值，通过自定义公式处理成想要在平台上展示与使用的值。比如固定值补偿、变量值成倍扩大或缩小、将4-20mA或0-5V测量值工程转换等。</p><p class="ql-align-justify">经过公式运算后，平台上只显示和起效此变量处理后的值，如：此变量在设备监控中列表和组态中展示的值、历史报表中的值、报警值等都是公式处理后的值。</p><p class="ql-align-justify">2、<strong style="color: rgb(51, 51, 51); background-color: rgb(255, 255, 255);">写入公式</strong><span style="color: rgb(51, 51, 51); background-color: rgb(255, 255, 255);">功能是采集公式功能的逆过程，它可以把在平台上输入框中输入的值，通过自定义公式处理成想要写入设备中的值。网关会把处理后的值，写入到设备中。</span></p><p><span style="color: rgb(51, 51, 51); background-color: rgb(255, 255, 255);">3、</span>公式输入格式</p><p class="ql-align-justify">1)公式遵循的原理是整个公式输入框中的内容为f(A),最终输出的值为d=f（A）（<strong>注意：A为大写英文字母，公式输入时不需要写“d=”</strong>）。</p><p class="ql-align-justify">2)<strong>采集公式</strong>中，X为网关采集到的原始值，公式处理后输出的值是在平台上展示的值。</p><p class="ql-align-justify">3)<strong>写入公式</strong>中，X为在平台写入框中写入的值，公式处理后输出的值会直接写入到设备中。</p><p class="ql-align-justify">4)在输入公式的过程中请保持所有输入均为<strong>英文半角字符输入</strong>，不能出现汉字符号与文字。</p><p class="ql-align-justify">5)采集公式示例：(0.5*A-40)/16&nbsp;&nbsp;&nbsp;输入公式示例：(16*A+40)*2。</p><p>4、支持的基本运算符：<strong>加(+)、减(-)、乘(*)、除(/)、取余(%)</strong>，为保证数据的正确性，请您严格按照实际公式运算符填写！</p>'
}

//计算公式计算
tipUtil.checkData=function(formula){
      console.log("计算公式",formula);
      let nums = []
      let input = formula
      let result = 0
      const operate1 = ['*', '/']
      const operate2 = ['+', '-']
      const operate3 = ['%']
      const calc1 = (str) => {
          let n = 0
          for (let i = 0; i < str.length; i++) {
              const element = str[i];
              if (operate1.indexOf(element) != -1||operate2.indexOf(element) != -1||operate3.indexOf(element) != -1) {
                  if (element == '*') {
                      result = parseFloat(nums[n]) * parseFloat(nums[n + 1])                                     
                      input = input.replace(nums[n] + '*' + nums[n + 1], '' + result)
                      nums.splice(n, 2, result)                
                      console.log("*",result);
                      n--
                  } else if (element == '/') {
                    result = parseFloat(nums[n]) / parseFloat(nums[n + 1])
                      input = input.replace(nums[n] + '/' + nums[n + 1], '' + result)
                      nums.splice(n, 2, result)
                      console.log("/",result);
                      n--
                  }
                  n++
              }
          }
          return result
      }
      const calc2 = (str) => {
          let n = 0
          for (let i = 0; i < str.length; i++) {
              const element = str[i];
              if (operate1.indexOf(element) != -1||operate2.indexOf(element) != -1||operate3.indexOf(element) != -1) {    
                  if (element == '+') {
                      result = parseFloat(nums[n]) + parseFloat(nums[n + 1])
                      input = input.replace(nums[n] + '+' + nums[n + 1], '' + result)
                      nums.splice(n, 2, result)
                      console.log("+",result);
                      n--
                  } else if (element == '-') {
                      result = parseFloat(nums[n]) - parseFloat(nums[n + 1])
                      input = input.replace(nums[n] + '-' + nums[n + 1], '' + result)
                      nums.splice(n, 2, result)
                      console.log("-",result);
                      n--
                  }
                  n++
              }
          }

          return result
      }
        const calc3 = (str) => {
          let n = 0
          for (let i = 0; i < str.length; i++) {
              const element = str[i];
              if (operate1.indexOf(element) != -1||operate2.indexOf(element) != -1||operate3.indexOf(element) != -1) {
                  if (element == '%') {         
                    result = parseFloat(nums[n]) % parseFloat(nums[n + 1])             
                    input = input.replace(nums[n] + '%' + nums[n + 1], '' + result)
                    nums.splice(n, 2, result)
                    console.log("%",result);
                    n--
                }
                  n++
              }
          }
          return result
      }

      input.split(/[%*/+-]/).forEach(element => {
        nums.push(element)
      });   
      console.log("数值数组",nums);
      result = calc3(input) 
      result = calc1(input)
      result = calc2(input)
      console.log("计算结果",result);
      
      return result;
}
//获取服务器IP:81.68.197.219、localhost
//this.tipUtil.getServerIp()
tipUtil.getServerIp=function(){
  var ip = window.location.host.split(':');
  return ip[0];
}
tipUtil.getServerPort=function(){
  return '11062'
}
export default tipUtil;
