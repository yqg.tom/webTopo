import JSEncrypt from 'jsencrypt/bin/jsencrypt.min'

// 密钥对生成 http://web.chacuo.net/netrsakeypair

const publicKey = 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMy4C7VhD7bq2cSEQOCsvVxRo4zH8ZUY\n' +
  'CqAe12qGp/PXVcwRHhyERhN9WvIUlu1aYgaKWdv1Ol/Ya4w5pdB8FdMCAwEAAQ=='

const privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAzLgLtWEPturZxIRA\n" +
  "4Ky9XFGjjMfxlRgKoB7Xaoan89dVzBEeHIRGE31a8hSW7VpiBopZ2/U6X9hrjDml\n" +
  "0HwV0wIDAQABAkB31Pu1jj0+mzYhnDDF0xd85KTRkXBOD6aYl7UMvAmv5z4GtMeU\n" +
  "1hmjVU8nN/gfia7oxg12jNpgSO3HbHlBJuvRAiEA8UI4qj3PlrOfuiKwKT5KRani\n" +
  "ZmgSBDX9VoccmPNdpd0CIQDZOkSf2W+ge8fG0UsZ4lrgCFOBfvMguKsdPTURyoan\n" +
  "bwIhALgy/DqWqMkB6cDevSswINrhYzcW3DKU2hDXZbEtlmGNAiAD2SVoE4kHtcmn\n" +
  "OwPylHD5sQwRqjcSaFMXtILhKs5R3wIhAJ9ZiUZW611RXX8wTkVgJBGzxptmFoUm\n" +
  "7ht1Oe/FE4bY";

// 加密
export function encrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPublicKey(publicKey) // 设置公钥
  return encryptor.encrypt(txt) // 对数据进行加密
}

// 解密
export function decrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPrivateKey(privateKey) // 设置私钥
  return encryptor.decrypt(txt) // 对数据进行解密
}

