import request from '@/utils/request'

// 查询图大全列表
export function listBEchartType(query) {
  return request({
    url: '/ghxx/bEchartType/list',
    method: 'get',
    params: query
  })
}

// 查询图大全详细
export function getBEchartType(id) {
  return request({
    url: '/ghxx/bEchartType/' + id,
    method: 'get'
  })
}

// 新增图大全
export function addBEchartType(data) {
  return request({
    url: '/ghxx/bEchartType',
    method: 'post',
    data: data
  })
}

// 修改图大全
export function updateBEchartType(data) {
  return request({
    url: '/ghxx/bEchartType',
    method: 'put',
    data: data
  })
}

// 删除图大全
export function delBEchartType(id) {
  return request({
    url: '/ghxx/bEchartType/' + id,
    method: 'delete'
  })
}

// 导出图大全
export function exportBEchartType(query) {
  return request({
    url: '/ghxx/bEchartType/export',
    method: 'get',
    params: query
  })
}
export function saveEchartImage(data) {
  return request({
    url: '/ghxx/bEchartType/saveEchartImage',
    method: 'post',
    data: data
  })
}