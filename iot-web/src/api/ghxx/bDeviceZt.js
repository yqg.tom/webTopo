import request from '@/utils/request'

// 查询云组态列表
export function listBDeviceZt(query) {
  return request({
    url: '/ghxx/bDeviceZt/list',
    method: 'get',
    params: query
  })
}

// 查询云组态详细
export function getBDeviceZt(id) {
  return request({
    url: '/ghxx/bDeviceZt/' + id,
    method: 'get'
  })
}

// 新增云组态
export function addBDeviceZt(data) {
  return request({
    url: '/ghxx/bDeviceZt',
    method: 'post',
    data: data
  })
}

// 修改云组态
export function updateBDeviceZt(data) {
  return request({
    url: '/ghxx/bDeviceZt',
    method: 'put',
    data: data
  })
}

// 删除云组态
export function delBDeviceZt(id) {
  return request({
    url: '/ghxx/bDeviceZt/' + id,
    method: 'delete'
  })
}

// 导出云组态
export function exportBDeviceZt(query) {
  return request({
    url: '/ghxx/bDeviceZt/export',
    method: 'get',
    params: query
  })
}
// 复制云组态
export function copyZtData(data) {
    return request({
      url: '/ghxx/bDeviceZt/copyZtData',
      method: 'post',
      data: data
    })
  }
  // 授权设备
export function empowerDevice(data) {
  return request({
    url: '/ghxx/bDeviceZt/empowerDevice',
    method: 'post',
    data: data
  })
}
//打包部署
export function downloadZtZip(data) {
  return request({
    url: '/ghxx/bDeviceZt/downloadZtZip',
    method: 'post',
    data: data
  })
}