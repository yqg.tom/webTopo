import request from '@/utils/request'

// 查询设备实时数据列表
export function listBDeviceRealData(query) {
  return request({
    url: '/ghxx/bDeviceRealData/list',
    method: 'get',
    params: query
  })
}

// 查询设备实时数据详细
export function getBDeviceRealData(id) {
  return request({
    url: '/ghxx/bDeviceRealData/' + id,
    method: 'get'
  })
}

// 新增设备实时数据
export function addBDeviceRealData(data) {
  return request({
    url: '/ghxx/bDeviceRealData',
    method: 'post',
    data: data
  })
}

// 修改设备实时数据
export function updateBDeviceRealData(data) {
  return request({
    url: '/ghxx/bDeviceRealData',
    method: 'put',
    data: data
  })
}

// 删除设备实时数据
export function delBDeviceRealData(id) {
  return request({
    url: '/ghxx/bDeviceRealData/' + id,
    method: 'delete'
  })
}

// 导出设备实时数据
export function exportBDeviceRealData(query) {
  return request({
    url: '/ghxx/bDeviceRealData/export',
    method: 'get',
    params: query
  })
}

// 复制设备实时数据
export function copyData(data) {
  return request({
    url: '/ghxx/bDeviceRealData/copyData',
    method: 'post',
    data: data
  })
}
// 数据测试
export function dataTest(data) {
  return request({
    url: '/ghxx/bDeviceRealData/dataTest',
    method: 'post',
    data: data
  })
}
// 查询近两个小时变量曲线图
export function getVariableLine(query) {
  return request({
    url: '/ghxx/bDeviceRealData/getVariableLine',
    method: 'get',
    params: query
  })
}
// 查询多个变量时间段曲线图
export function getVariableLines(query) {
  return request({
    url: '/ghxx/bDeviceRealData/getVariableLines',
    method: 'get',
    params: query
  })
}
// 指令下发
export function orderSend(data) {
  return request({
    url: '/ghxx/bDeviceRealData/orderSend',
    method: 'post',
    data: data
  })
}
// 数据同步
export function dataSync(data) {
  return request({
    url: '/ghxx/bDeviceRealData/dataSync',
    method: 'post',
    data: data
  })
}