import request from '@/utils/request'

// 查询大屏管理列表
export function listBEchartZt(query) {
  return request({
    url: '/ghxx/bEchartZt/list',
    method: 'get',
    params: query
  })
}

// 查询大屏管理详细
export function getBEchartZt(id) {
  return request({
    url: '/ghxx/bEchartZt/' + id,
    method: 'get'
  })
}

// 新增大屏管理
export function addBEchartZt(data) {
  return request({
    url: '/ghxx/bEchartZt',
    method: 'post',
    data: data
  })
}

// 修改大屏管理
export function updateBEchartZt(data) {
  return request({
    url: '/ghxx/bEchartZt',
    method: 'put',
    data: data
  })
}

// 删除大屏管理
export function delBEchartZt(id) {
  return request({
    url: '/ghxx/bEchartZt/' + id,
    method: 'delete'
  })
}

// 导出大屏管理
export function exportBEchartZt(query) {
  return request({
    url: '/ghxx/bEchartZt/export',
    method: 'get',
    params: query
  })
}