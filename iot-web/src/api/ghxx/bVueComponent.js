import request from '@/utils/request'

// 查询自定义组件列表
export function listBVueComponent(query) {
  return request({
    url: '/ghxx/bVueComponent/list',
    method: 'get',
    params: query
  })
}

// 查询自定义组件详细
export function getBVueComponent(id) {
  return request({
    url: '/ghxx/bVueComponent/' + id,
    method: 'get'
  })
}

// 新增自定义组件
export function addBVueComponent(data) {
  return request({
    url: '/ghxx/bVueComponent',
    method: 'post',
    data: data
  })
}

// 修改自定义组件
export function updateBVueComponent(data) {
  return request({
    url: '/ghxx/bVueComponent',
    method: 'put',
    data: data
  })
}

// 删除自定义组件
export function delBVueComponent(id) {
  return request({
    url: '/ghxx/bVueComponent/' + id,
    method: 'delete'
  })
}

// 导出自定义组件
export function exportBVueComponent(query) {
  return request({
    url: '/ghxx/bVueComponent/export',
    method: 'get',
    params: query
  })
}
export function saveComponentImage(data) {
  return request({
    url: '/ghxx/bVueComponent/saveComponentImage',
    method: 'post',
    data: data
  })
}