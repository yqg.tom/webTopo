import request from '@/utils/request'

// 查询三维配置列表
export function listBDeviceModel(query) {
  return request({
    url: '/ghxx/bDeviceModel/list',
    method: 'get',
    params: query
  })
}

// 查询三维配置详细
export function getBDeviceModel(id) {
  return request({
    url: '/ghxx/bDeviceModel/' + id,
    method: 'get'
  })
}

// 新增三维配置
export function addBDeviceModel(data) {
  return request({
    url: '/ghxx/bDeviceModel',
    method: 'post',
    data: data
  })
}

// 修改三维配置
export function updateBDeviceModel(data) {
  return request({
    url: '/ghxx/bDeviceModel',
    method: 'put',
    data: data
  })
}

// 删除三维配置
export function delBDeviceModel(id) {
  return request({
    url: '/ghxx/bDeviceModel/' + id,
    method: 'delete'
  })
}

// 导出三维配置
export function exportBDeviceModel(query) {
  return request({
    url: '/ghxx/bDeviceModel/export',
    method: 'get',
    params: query
  })
}