import request from '@/utils/request'

// 查询设备日志列表
export function listHistory(query) {
  return request({
    url: '/ghxx/history/list',
    method: 'get',
    params: query
  })
}

// 查询设备日志详细
export function getHistory(id) {
  return request({
    url: '/ghxx/history/' + id,
    method: 'get'
  })
}

// 新增设备日志
export function addHistory(data) {
  return request({
    url: '/ghxx/history',
    method: 'post',
    data: data
  })
}

// 修改设备日志
export function updateHistory(data) {
  return request({
    url: '/ghxx/history',
    method: 'put',
    data: data
  })
}

// 删除设备日志
export function delHistory(id) {
  return request({
    url: '/ghxx/history/' + id,
    method: 'delete'
  })
}

// 导出设备日志
export function exportHistory(query) {
  return request({
    url: '/ghxx/history/export',
    method: 'get',
    params: query
  })
}

// 导出【根据不同条件】
export function exportOfParam(query) {
  return request({
    url: '/ghxx/history/monitorExport',
    method: 'get',
    params: query
  })
}

// 统计报表
export function statistics(data) {
  return request({
    url: '/ghxx/history/getSurfaceStat', 
    method: 'post',
    data: data
  })
}