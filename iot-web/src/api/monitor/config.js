import request from '@/utils/request'

// 查询告警管理配置列表
export function listConfig(query) {
  return request({
    url: '/ghxx/config/list',
    method: 'get',
    params: query
  })
}

// 查询告警管理配置详细
export function getConfig(id) {
  return request({
    url: '/ghxx/config/' + id,
    method: 'get'
  })
}

// 新增告警管理配置
export function addConfig(data) {
  return request({
    url: '/ghxx/config',
    method: 'post',
    data: data
  })
}

// 修改告警管理配置
export function updateConfig(data) {
  return request({
    url: '/ghxx/config',
    method: 'put',
    data: data
  })
}

// 删除告警管理配置
export function delConfig(id) {
  return request({
    url: '/ghxx/config/' + id,
    method: 'delete'
  })
}

// 导出告警管理配置
export function exportConfig(query) {
  return request({
    url: '/ghxx/config/export',
    method: 'get',
    params: query
  })
}

// 预警已选项目
export function chooseConfig(id) {
  return request({
    url: '/ghxx/config/' + id,
    method: 'put'
  })
}