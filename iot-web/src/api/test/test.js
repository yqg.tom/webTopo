import request from '@/utils/request'

// 查询生成表数据
export function listTable(data) {
  return request({
    url: '/ghxx/texteditor/list',
    method: 'get',
    params: data
  })
}
//保存或修改文本编辑器的内容
  export function save(data) {
    return request({
      url: '/ghxx/texteditor/save',
      method: 'put',
      data: data
    })
  }
