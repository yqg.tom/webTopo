import request from '@/utils/request'

// 查询上传信息列表
export function listUpload(query) {
  return request({
    url: '/ghxx/upload/list',
    method: 'get',
    params: query
  })
}

// 查询上传信息详细
export function getUpload(id) {
  return request({
    url: '/ghxx/upload/' + id,
    method: 'get'
  })
}

// 新增上传信息
export function addUpload(data) {
  return request({
    url: '/ghxx/upload',
    method: 'post',
    data: data
  })
}

// 修改上传信息
export function updateUpload(data) {
  return request({
    url: '/ghxx/upload',
    method: 'put',
    data: data
  })
}

// 删除上传信息
export function delUpload(id) {
  return request({
    url: '/ghxx/upload/' + id,
    method: 'delete'
  })
}

// 导出上传信息
export function exportUpload(query) {
  return request({
    url: '/ghxx/upload/export',
    method: 'get',
    params: query
  })
}