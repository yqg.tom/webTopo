import request from '@/utils/request'

// 查询mqtt上报日志列表
export function listBMqttMessage(query) {
  return request({
    url: '/synthesize/bMqttMessage/list',
    method: 'get',
    params: query
  })
}

// 查询mqtt上报日志详细
export function getBMqttMessage(id) {
  return request({
    url: '/synthesize/bMqttMessage/' + id,
    method: 'get'
  })
}

// 新增mqtt上报日志
export function addBMqttMessage(data) {
  return request({
    url: '/synthesize/bMqttMessage',
    method: 'post',
    data: data
  })
}

// 修改mqtt上报日志
export function updateBMqttMessage(data) {
  return request({
    url: '/synthesize/bMqttMessage',
    method: 'put',
    data: data
  })
}

// 删除mqtt上报日志
export function delBMqttMessage(id) {
  return request({
    url: '/synthesize/bMqttMessage/' + id,
    method: 'delete'
  })
}

// 导出mqtt上报日志
export function exportBMqttMessage(query) {
  return request({
    url: '/synthesize/bMqttMessage/export',
    method: 'get',
    params: query
  })
}