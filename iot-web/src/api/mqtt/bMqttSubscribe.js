import request from '@/utils/request'

// 查询mqtt主题订阅列表
export function listBMqttSubscribe(query) {
  return request({
    url: '/synthesize/bMqttSubscribe/list',
    method: 'get',
    params: query
  })
}

// 查询mqtt主题订阅详细
export function getBMqttSubscribe(id) {
  return request({
    url: '/synthesize/bMqttSubscribe/' + id,
    method: 'get'
  })
}

// 新增mqtt主题订阅
export function addBMqttSubscribe(data) {
  return request({
    url: '/synthesize/bMqttSubscribe',
    method: 'post',
    data: data
  })
}

// 修改mqtt主题订阅
export function updateBMqttSubscribe(data) {
  return request({
    url: '/synthesize/bMqttSubscribe',
    method: 'put',
    data: data
  })
}

// 删除mqtt主题订阅
export function delBMqttSubscribe(id) {
  return request({
    url: '/synthesize/bMqttSubscribe/' + id,
    method: 'delete'
  })
}

// 导出mqtt主题订阅
export function exportBMqttSubscribe(query) {
  return request({
    url: '/synthesize/bMqttSubscribe/export',
    method: 'get',
    params: query
  })
}