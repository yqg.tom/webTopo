package com.ruoyi.ghxx.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.mapper.BDeviceModelMapper;
import com.ruoyi.ghxx.domain.BDeviceModel;
import com.ruoyi.ghxx.service.IBDeviceModelService;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
/**
 * 三维配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Service
public class BDeviceModelServiceImpl implements IBDeviceModelService 
{
    @Autowired
    private BDeviceModelMapper bDeviceModelMapper;

    /**
     * 查询三维配置
     * 
     * @param id 三维配置主键
     * @return 三维配置
     */
    @Override
    public BDeviceModel selectBDeviceModelById(Long id)
    {
        return bDeviceModelMapper.selectBDeviceModelById(id);
    }

    /**
     * 查询三维配置列表
     * 
     * @param bDeviceModel 三维配置
     * @return 三维配置
     */
    @Override
    public List<BDeviceModel> selectBDeviceModelList(BDeviceModel bDeviceModel)
    {
        return bDeviceModelMapper.selectBDeviceModelList(bDeviceModel);
    }

    /**
     * 新增三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    @Override
    public int insertBDeviceModel(BDeviceModel bDeviceModel)
    {
        Snowflake snowflake = IdUtil.getSnowflake(6, 2);
        long id = snowflake.nextId();
        bDeviceModel.setId(id);
        return bDeviceModelMapper.insertBDeviceModel(bDeviceModel);
    }

    /**
     * 修改三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    @Override
    public int updateBDeviceModel(BDeviceModel bDeviceModel)
    {
        return bDeviceModelMapper.updateBDeviceModel(bDeviceModel);
    }

    /**
     * 批量删除三维配置
     * 
     * @param ids 需要删除的三维配置主键
     * @return 结果
     */
    @Override
    public int deleteBDeviceModelByIds(Long[] ids)
    {
        return bDeviceModelMapper.deleteBDeviceModelByIds(ids);
    }

    /**
     * 删除三维配置信息
     * 
     * @param id 三维配置主键
     * @return 结果
     */
    @Override
    public int deleteBDeviceModelById(Long id)
    {
        return bDeviceModelMapper.deleteBDeviceModelById(id);
    }
}
