package com.ruoyi.ghxx.controller;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.text.Convert;;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.ghxx.domain.*;
import com.ruoyi.ghxx.mapper.BDeviceZtMapper;
import com.ruoyi.ghxx.service.*;
import com.ruoyi.ghxx.util.Detect;
import com.ruoyi.ghxx.util.HttpPostAndGetUtil;
import com.ruoyi.ghxx.vo.BDeviceToken;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * @author cl
 */
@RestController
@RequestMapping(value = "/ignoreToken")
@Anonymous
public class BDeviceBigDataController {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    public RedisTemplate redisTemplate;
    /**
     * 获取设备管理详细信息，-2已过期，-1没有设置有效期，0以上的值表示是这个key的剩余有效时间
     */
    @GetMapping(value = "/getTokenByImei")
    public AjaxResult getTokenByImei(BDeviceToken bDeviceToken) {
        String token="";
        BDeviceZt bDeviceZt=new BDeviceZt();
        String ztSql="select * from b_device_zt where guid=?";
        BeanPropertyRowMapper<BDeviceZt> ztBeanPropertyRowMapper = new BeanPropertyRowMapper<>(BDeviceZt.class);
        try {
            bDeviceZt = jdbcTemplate.queryForObject(ztSql, ztBeanPropertyRowMapper, bDeviceToken.getZtGuid());
        }catch (EmptyResultDataAccessException e){
            System.out.println("无组态信息");
            return AjaxResult.error("token获取失败");
        }
        String deptIdStrs=bDeviceZt.getDeptIdStrs();
        String[] split = deptIdStrs.split(",");
        List<Long> integerList=new ArrayList<>();
        for (int i = 0; i < split.length; i++) {
            if(Convert.toInt(split[i])!=null){
                integerList.add(Convert.toLong(split[i]));
            }
        }
        Long deptId = Collections.max(integerList);
        String sql="select * from sys_user where status=0 and dept_id=? and del_flag !='2'limit 1";
        BeanPropertyRowMapper<SysUser> userBeanPropertyRowMapper = new BeanPropertyRowMapper<>(SysUser.class);
        try {
            SysUser sysUser = jdbcTemplate.queryForObject(sql, userBeanPropertyRowMapper, deptId);
            LoginUser loginUser=(LoginUser) userDetailsService.loadUserByUsername(sysUser.getUserName());
            token = tokenService.createToken(loginUser);
        }catch (EmptyResultDataAccessException e){
            System.out.println("无用户信息");
            return AjaxResult.error("token获取失败");
        }
        return AjaxResult.success("token获取成功",token);
    }
    /**
     * 自定义地图数据
     */
    @GetMapping("/getMapJson")
    public AjaxResult getMapJson(String url) throws Exception {
        String mapJson = HttpPostAndGetUtil.httpsDoGet(url);
        return AjaxResult.success(mapJson);
    }
}
