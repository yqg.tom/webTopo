package com.ruoyi.ghxx.vo;

import lombok.Data;

import java.util.List;

@Data
public class BDeviceEcharts {
    private List<Integer> timeList;

    private List<String> nameList;
    private List<List<Object>> anaList;
}
