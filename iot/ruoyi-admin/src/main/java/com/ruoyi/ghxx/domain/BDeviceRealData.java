package com.ruoyi.ghxx.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ruoyi.ghxx.vo.BDeviceParam;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备实时数据对象 b_device_real_data
 * 
 * @author ruoyi
 * @date 2022-05-27
 */
@Data
public class BDeviceRealData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** 参数名称 */
    @Excel(name = "参数名称")
    private String paramName;

    /** 参数字段 */
    @Excel(name = "参数字段")
    private String paramField;

    /** 参数值 */
    @Excel(name = "参数值")
    private String paramValue;

    /** 参数单位 */
    @Excel(name = "参数单位")
    private String paramUnit;

    /** 组件Guid */
    @Excel(name = "组件Guid")
    private String ztGuid;


}
