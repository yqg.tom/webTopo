package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 大屏管理对象 b_echart_zt
 * 
 * @author ruoyi
 * @date 2023-05-24
 */
@Data
public class BEchartZt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id唯一标识 */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** 页面名称 */
    @Excel(name = "页面名称")
    private String pageName;

    /** 页面图片 */
    private String pageImage;

    /** 页面数据 */
    private String pageData;

    private String base64;

}
