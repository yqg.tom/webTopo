package com.ruoyi.ghxx.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备升级对象 b_device_upgrade
 * 
 * @author ruoyi
 * @date 2021-12-21
 */
@Data
public class BDeviceUpgradeVo implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String deviceImei;

    /** 设备版本 */
    @Excel(name = "设备版本")
    private String deviceVersion;

    /** 升级文件 */
    @Excel(name = "升级文件")
    private String deviceFile;

    /** 升级路径 */
    @Excel(name = "升级路径")
    private String devicePath;

    /** 升级版本 */
    @Excel(name = "升级版本")
    private String upgradeVersion;
}
