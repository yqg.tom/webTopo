package com.ruoyi.ghxx.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.ghxx.aop.SysInitData;
import com.ruoyi.ghxx.aop.SysInitUpdateData;
import com.ruoyi.ghxx.aop.SysQueryData;
import com.ruoyi.ghxx.domain.BDeviceZt;
import com.ruoyi.ghxx.modelVo.BHttpRequest;
import com.ruoyi.ghxx.util.Detect;
import com.ruoyi.ghxx.util.HttpPostAndGetUtil;
import com.ruoyi.ghxx.util.ImageUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.ghxx.domain.BEchartType;
import com.ruoyi.ghxx.service.IBEchartTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 图大全Controller
 * 
 * @author ruoyi
 * @date 2023-05-25
 */
@RestController
@RequestMapping("/ghxx/bEchartType")
public class BEchartTypeController extends BaseController
{
    @Autowired
    private IBEchartTypeService bEchartTypeService;

    /**
     * 查询图大全列表
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:list')")
    @GetMapping("/list")
//    @SysQueryData
    public TableDataInfo list(BEchartType bEchartType)
    {
        startPage();
        List<BEchartType> list = bEchartTypeService.selectBEchartTypeList(bEchartType);
        return getDataTable(list);
    }

    /**
     * 导出图大全列表
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:export')")
    @Log(title = "图大全", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BEchartType bEchartType)
    {
        List<BEchartType> list = bEchartTypeService.selectBEchartTypeList(bEchartType);
        ExcelUtil<BEchartType> util = new ExcelUtil<BEchartType>(BEchartType.class);
        util.exportExcel(response, list, "图大全数据");
    }

    /**
     * 获取图大全详细信息
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(bEchartTypeService.selectBEchartTypeById(id));
    }

    /**
     * 新增图大全
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:add')")
    @Log(title = "图大全", businessType = BusinessType.INSERT)
    @PostMapping
    @SysInitData
    public AjaxResult add(@RequestBody BEchartType bEchartType)
    {
        return toAjax(bEchartTypeService.insertBEchartType(bEchartType));
    }

    /**
     * 修改图大全
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:edit')")
    @Log(title = "图大全", businessType = BusinessType.UPDATE)
    @PutMapping
    @SysInitUpdateData
    public AjaxResult edit(@RequestBody BEchartType bEchartType)
    {

        return toAjax(bEchartTypeService.updateBEchartType(bEchartType));
    }

    /**
     * 删除图大全
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bEchartType:remove')")
    @Log(title = "图大全", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bEchartTypeService.deleteBEchartTypeByIds(ids));
    }
    /*
     * 保存组态缩略图
     * */
    @PostMapping(value = "/saveEchartImage")
    public AjaxResult saveEchartImage(@RequestBody BEchartType bEchartType)
    {
        MultipartFile image =ImageUtils.base64ToMultipartFile(bEchartType.getBase64());
        String imageUrl="";
        try{
            imageUrl = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), image);
        }catch (Exception e){
            return AjaxResult.error(500,"上传组件异常，"+e.toString());
        }
        if(Detect.notEmpty(bEchartType.getEchartImgae())){
            String newUrl = bEchartType.getEchartImgae().replace("/profile", "").replace("/avatar", "");
            File fileQuery=new File(RuoYiConfig.getAvatarPath()+newUrl);
            if(fileQuery.exists()){
                System.out.println("删除之前的图片资源");
                fileQuery.delete();
            }
        }
        bEchartType.setEchartImgae(imageUrl);
        bEchartTypeService.updateBEchartType(bEchartType);
        return AjaxResult.success();
    }
    /*
     * 发送自定义http请求
     * */
    @PostMapping(value = "/sendHttpRequest")
    public AjaxResult sendHttpRequest(@RequestBody BHttpRequest bHttpRequest)
    {
        String jsonStr = bEchartTypeService.sendHttpRequest(bHttpRequest);
        JSONObject jsonObject = JSONObject.parseObject(jsonStr,JSONObject.class);
        return AjaxResult.success("200",jsonObject);
    }
}
