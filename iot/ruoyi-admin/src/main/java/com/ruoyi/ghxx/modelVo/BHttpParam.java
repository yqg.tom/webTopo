package com.ruoyi.ghxx.modelVo;

import lombok.Data;

@Data
public class BHttpParam {
    private String key;
    private String value;
}
