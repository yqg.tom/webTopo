package com.ruoyi.ghxx.service;

import java.util.List;

import com.ruoyi.ghxx.domain.BDeviceRealData;

/**
 * 设备实时数据Service接口
 * 
 * @author ruoyi
 * @date 2022-05-27
 */
public interface IBDeviceRealDataService 
{
    /**
     * 查询设备实时数据
     * 
     * @param id 设备实时数据ID
     * @return 设备实时数据
     */
    public BDeviceRealData selectBDeviceRealDataById(Long id);

    /**
     * 查询设备实时数据列表
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 设备实时数据集合
     */
    public List<BDeviceRealData> selectBDeviceRealDataList(BDeviceRealData bDeviceRealData);

    /**
     * 新增设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    public Integer insertBDeviceRealData(BDeviceRealData bDeviceRealData);

    /**
     * 修改设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    public Integer updateBDeviceRealData(BDeviceRealData bDeviceRealData);

    /**
     * 批量删除设备实时数据
     * 
     * @param ids 需要删除的设备实时数据ID
     * @return 结果
     */
    public int deleteBDeviceRealDataByIds(Long[] ids);

    /**
     * 删除设备实时数据信息
     * 
     * @param id 设备实时数据ID
     * @return 结果
     */
    public int deleteBDeviceRealDataById(Long id);
}
