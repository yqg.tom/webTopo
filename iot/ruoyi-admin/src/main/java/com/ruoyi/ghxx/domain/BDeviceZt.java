package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 云组态对象 b_device_zt
 * 
 * @author ruoyi
 * @date 2022-05-21
 */
@Data
public class BDeviceZt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id唯一标识 */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** 部门id串(like查询) */
    private String deptIdStrs;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String deviceMac;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** $column.columnComment */
    @Excel(name = "组态信息")
    private String deviceZtData;
    private Boolean isCache=false;
    /** 是否主界面 */
    @Excel(name = "是否主界面")
    private Integer isMainPage;

    /** 页面名称 */
    @Excel(name = "页面名称")
    private String pageName;

    /** 页面大小 */
    private String pageSize;

    /** 是否分享 */
    private Integer isShare;

    private String shareUrl;

    /** 分享密码 */
    private String sharePass;
    /** 页面图片 */
    private String pageImage;
    private Boolean isOnline=true;
    private String paramDoubleQuery;
    private String queryDeptId;
    private String base64;
}
