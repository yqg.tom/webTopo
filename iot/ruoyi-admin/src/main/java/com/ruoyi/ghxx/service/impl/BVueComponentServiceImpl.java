package com.ruoyi.ghxx.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.mapper.BVueComponentMapper;
import com.ruoyi.ghxx.domain.BVueComponent;
import com.ruoyi.ghxx.service.IBVueComponentService;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
/**
 * 自定义组件Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
@Service
public class BVueComponentServiceImpl implements IBVueComponentService 
{
    @Autowired
    private BVueComponentMapper bVueComponentMapper;

    /**
     * 查询自定义组件
     * 
     * @param id 自定义组件主键
     * @return 自定义组件
     */
    @Override
    public BVueComponent selectBVueComponentById(Long id)
    {
        return bVueComponentMapper.selectBVueComponentById(id);
    }

    /**
     * 查询自定义组件列表
     * 
     * @param bVueComponent 自定义组件
     * @return 自定义组件
     */
    @Override
    public List<BVueComponent> selectBVueComponentList(BVueComponent bVueComponent)
    {
        return bVueComponentMapper.selectBVueComponentList(bVueComponent);
    }

    /**
     * 新增自定义组件
     * 
     * @param bVueComponent 自定义组件
     * @return 结果
     */
    @Override
    public int insertBVueComponent(BVueComponent bVueComponent)
    {
        Snowflake snowflake = IdUtil.getSnowflake(6, 2);
        long id = snowflake.nextId();
        bVueComponent.setId(id);
        bVueComponent.setCreateTime(DateUtils.getNowDate());
        return bVueComponentMapper.insertBVueComponent(bVueComponent);
    }

    /**
     * 修改自定义组件
     * 
     * @param bVueComponent 自定义组件
     * @return 结果
     */
    @Override
    public int updateBVueComponent(BVueComponent bVueComponent)
    {
        bVueComponent.setUpdateTime(DateUtils.getNowDate());
        return bVueComponentMapper.updateBVueComponent(bVueComponent);
    }

    /**
     * 批量删除自定义组件
     * 
     * @param ids 需要删除的自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteBVueComponentByIds(Long[] ids)
    {
        return bVueComponentMapper.deleteBVueComponentByIds(ids);
    }

    /**
     * 删除自定义组件信息
     * 
     * @param id 自定义组件主键
     * @return 结果
     */
    @Override
    public int deleteBVueComponentById(Long id)
    {
        return bVueComponentMapper.deleteBVueComponentById(id);
    }
}
