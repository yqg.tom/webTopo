package com.ruoyi.ghxx.aop;


import java.lang.annotation.*;

/**
 * 定义公共字段初始化注解
 * @author cl
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysInitUpdateData {
    String value() default "";
}
