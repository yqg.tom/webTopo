package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 三维配置对象 b_device_model
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Data
public class BDeviceModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    //long型转string，因为java和js对于long的位数不一致，会丢失精度，所以转换成string
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** 分配用户名 */
    private String userName;

    /** 分配用户名 */
    @Excel(name = "分配用户名")
    private String nickName;

    /** 组织名称 */
    @Excel(name = "组织名称")
    private String deptName;

    /** 模型名称 */
    @Excel(name = "模型名称")
    private String modelName;

    /** 模型地址 */
    @Excel(name = "模型地址")
    private String modelUrl;

    /** 是否弃用 */
    @Excel(name = "是否弃用")
    private Integer status;
    /** 缩略图url */
    @Excel(name = "缩略图url")
    private String imageUrl;

}
