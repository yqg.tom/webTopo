package com.ruoyi.ghxx.service;

import java.util.List;
import com.ruoyi.ghxx.domain.BEchartType;
import com.ruoyi.ghxx.modelVo.BHttpRequest;

/**
 * 图大全Service接口
 * 
 * @author ruoyi
 * @date 2023-05-25
 */
public interface IBEchartTypeService 
{
    /**
     * 查询图大全
     * 
     * @param id 图大全主键
     * @return 图大全
     */
    public BEchartType selectBEchartTypeById(Long id);

    /**
     * 查询图大全列表
     * 
     * @param bEchartType 图大全
     * @return 图大全集合
     */
    public List<BEchartType> selectBEchartTypeList(BEchartType bEchartType);

    /**
     * 新增图大全
     * 
     * @param bEchartType 图大全
     * @return 结果
     */
    public int insertBEchartType(BEchartType bEchartType);

    /**
     * 修改图大全
     * 
     * @param bEchartType 图大全
     * @return 结果
     */
    public int updateBEchartType(BEchartType bEchartType);

    /**
     * 批量删除图大全
     * 
     * @param ids 需要删除的图大全主键集合
     * @return 结果
     */
    public int deleteBEchartTypeByIds(Long[] ids);

    /**
     * 删除图大全信息
     * 
     * @param id 图大全主键
     * @return 结果
     */
    public int deleteBEchartTypeById(Long id);
    /*
    * 发送自定义http请求
    * */
    public String sendHttpRequest(BHttpRequest bHttpRequest);
}
