package com.ruoyi.ghxx.modelVo;

import lombok.Data;

import java.util.List;

@Data
public class BHttpRequest {
    private String url;
    private String method;
    private List<BHttpParam> params;
    private List<BHttpParam> headers;
    private String data;
    private String contentType;
}
