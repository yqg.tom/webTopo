package com.ruoyi.ghxx.util;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.util.*;

/**
 *
 * @author neptune
 *
 */
public abstract class Detect {

	public static final short INVALID_NUMBER_VALUE = 0;

	public static final String EMPTY_STRING = "";

	public static final String DELIMITER = ",";

	/** is */
	public static boolean isEmpty(long[] array) {
		return null == array || array.length == 0;
	}

	public static boolean isNegative(double value) {
		return value < 0;
	}

	public static boolean isPositive(double value) {
		return value > 0;
	}
	
	public static boolean isPositive(Long value) {
		return null!=value && value.longValue() > 0;
	}
	
	public static boolean isTrue(Boolean value) {
		return Boolean.TRUE.equals(value);
	}

	public static boolean isFalse(Boolean value) {
		return Boolean.FALSE.equals(value);
	}

	public static boolean isEmpty(String string) {
		return null == string || EMPTY_STRING.equals(string);
	}

	public static boolean notNull(Object value) {
		return null != value;
	}

	public static boolean notEmpty(String string) {
		return null != string && !EMPTY_STRING.equals(string);
	}

	public static boolean notEmpty(Collection<?> collection) {

		if (null != collection) {
			return collection.size() > 0;
		}
		return false;
	}

	public static boolean notEmpty(Map<?, ?> map) {
		return null != map && !map.isEmpty();
	}

	public static <T> boolean notEmpty(T[] array) {
		return null != array && array.length > 0;
	}

	public static boolean notEmpty(byte[] array) {
		return null != array && array.length > 0;
	}

	public static boolean notEmpty(short[] array) {
		return null != array && array.length > 0;
	}

	public static boolean notEmpty(int[] array) {
		return null != array && array.length > 0;
	}

	public static boolean notEmpty(long[] array) {
		return !isEmpty(array);
	}

	public static boolean equals(Long a, Long b) {
		if (null == a && null == b)
			return true;

		if (null != a && null != b && a.longValue() == b.longValue())
			return true;

		return false;
	}

	/**  */
	public static double max(double a, double b) {
		return (a > b) ? a : b;
	}

	public static double max(double... values) {
		// TBD
		return 0;
	}

	public static double min(double a, double b) {
		return (a < b) ? a : b;
	}

	public static double min(double... values) {
		// TBD
		return 0;
	}

	/** equals */
	public static boolean equals(String string1, String string2) {
		return StringUtils.equals(string1, string2);
	}

	public static boolean equals(Class<?> clazz1, Class<?> clazz2) {
		return clazz1 == null ? clazz2 == null : clazz1.equals(clazz2);
	}

	public static boolean equals(Object object1, Object object2) {
		return object1 == null ? object2 == null : object1.equals(object2);
	}

	public static boolean equals(Boolean boolean1, Boolean boolean2) {
		return boolean1 == null ? boolean2 == null : boolean1.booleanValue() == boolean2.booleanValue();
	}

	/** contains */
	public static boolean contains(long value, long[] values) {
		if (notEmpty(values)) {
			for (long v : values) {
				if (v == value) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean contains(short value, short[] values) {
		if (notEmpty(values)) {
			for (long v : values) {
				if (v == value) {
					return true;
				}
			}
		}
		return false;
	}

	public static int compareTo(String s1, String s2) {
		if (null == s1 && null == s2) {
			return 0;
		}
		if (null == s1) {
			return 1;
		}
		if (null == s2) {
			return -1;
		}
		return s1.compareTo(s2);
	}

	public static <E> boolean contains(E one, List<E> list) {
		if (notEmpty(list) && null != one) {
			for (E item : list) {
				if (one.equals(item)) {
					return true;
				}
			}
		}
		return false;
	}

	public static <E> boolean contains(E one, E[] array) {
		if (notEmpty(array) && null != one) {
			// return Arrays.asList(many).contains(one);
			for (E item : array) {
				if (one.equals(item)) {
					return true;
				}
			}
		}
		return false;
	}

	/** *array */

	public static short[] shortArray(String value) {
		return shortArray(value, DELIMITER);
	}

	public static short[] shortArray(String value, char delimiter) {
		if (!notEmpty(value)) {
			return null;
		}
		String[] values = StringUtils.split(value, delimiter);

		short[] shortValues = new short[values.length];
		for (int i = 0; i < values.length; i++) {
			shortValues[i] = Short.parseShort(values[i]);
		}
		return shortValues;
	}

	public static short[] shortArray(String value, String delimiter) {
		if (!notEmpty(value)) {
			return null;
		}
		String[] values = StringUtils.split(value, delimiter);

		short[] shortValues = new short[values.length];
		for (int i = 0; i < values.length; i++) {
			shortValues[i] = Short.parseShort(values[i]);
		}
		return shortValues;
	}

	public static int[] intArray(String value) {
		return intArray(value, DELIMITER);
	}

	public static int[] intArray(String value, String delimiter) {
		if (!notEmpty(value)) {
			return null;
		}
		String[] values = StringUtils.split(value, delimiter);

		int[] intValues = new int[values.length];
		for (int i = 0; i < values.length; i++) {
			intValues[i] = Integer.parseInt(values[i]);
		}
		return intValues;
	}

	public static long[] longArray(String value) {
		return longArray(value, DELIMITER);
	}

	public static long[] longArray(String value, char delimiter) {
		if (!notEmpty(value)) {
			return null;
		}
		String[] values = StringUtils.split(value, delimiter);

		long[] longValues = new long[values.length];
		for (int i = 0; i < values.length; i++) {
			longValues[i] = Long.parseLong(values[i]);
		}
		return longValues;
	}

	public static long[] longArray(String value, String delimiter) {
		if (!notEmpty(value)) {
			return null;
		}
		String[] values = StringUtils.split(value, delimiter);

		long[] longValues = new long[values.length];
		for (int i = 0; i < values.length; i++) {
			longValues[i] = Long.parseLong(values[i]);
		}
		return longValues;
	}

	public static double[] doubleArray(String value) {
		return doubleArray(value, DELIMITER);
	}

	public static double[] doubleArray(String value, String delimiter) {
		String[] values = StringUtils.split(value, delimiter);

		double[] doubleValues = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			doubleValues[i] = Double.parseDouble(values[i]);
		}
		return doubleValues;
	}

	/** size */
	public static int size(List<?> list) {
		return notEmpty(list) ? list.size() : 0;
	}

	// public static int size(T[] array) {
	// return notEmpty(array) ? array.length : 0;
	// }

	/** grouping */
	public static List<long[]> grouping(long[] values, int size) {
		if (notEmpty(values)) {

			//Assertion.isPositive(size, "size must be bigger than 0");

			int groupLength = values.length / size + ((values.length % size) > 0 ? 1 : 0);

			List<long[]> longArryGroup = new LinkedList<long[]>();
			long[] valueArray = null;
			for (int i = 0; i < groupLength; i++) {
				int arrayLength = (i < groupLength - 1 || values.length % size == 0) ? size : (values.length % size);

				valueArray = new long[arrayLength];
				for (int j = 0; j < arrayLength; j++) {
					valueArray[j] = values[i * size + j];
				}
				longArryGroup.add(valueArray);
			}

			return longArryGroup;
		}
		return null;
	}

	public static List<String[]> grouping(String[] values, int size) {
		if (notEmpty(values)) {

			//Assertion.isPositive(size, "size must be bigger than 0");

			int groupLength = values.length / size + ((values.length % size) > 0 ? 1 : 0);

			List<String[]> longArryGroup = new LinkedList<String[]>();
			String[] valueArray = null;
			for (int i = 0; i < groupLength; i++) {
				int arrayLength = (i < groupLength - 1 || values.length % size == 0) ? size : (values.length % size);

				valueArray = new String[arrayLength];
				for (int j = 0; j < arrayLength; j++) {
					valueArray[j] = values[i * size + j];
				}
				longArryGroup.add(valueArray);
			}

			return longArryGroup;
		}
		return null;
	}

	/** join */
	public static <K, V> String join(Map<K, V> values) {// toQueryString
		return "TBD";
	}

	public static String join(short[] values) {
		return join(values, DELIMITER);
	}

	public static String join(long[] values) {
		return join(values, DELIMITER);
	}

	public static String join(String[] values) {
		return StringUtils.join(values, DELIMITER);
	}

	public static String join(List<String> values) {
		return StringUtils.join(values, DELIMITER);
	}

	public static String join(short[] values, String delimiter) {
		return StringUtils.join(ArrayUtils.toObject(values), delimiter);
	}

	public static String join(long[] values, String delimiter) {
		return StringUtils.join(ArrayUtils.toObject(values), delimiter);
	}

	/** as */
	public static boolean asBoolean(Object value) {
		return (value instanceof Boolean) ? ((Boolean) value).booleanValue() : Boolean.parseBoolean(asString(value));
	}

	public static Boolean asWrapperBoolean(Object value) {
		return null == value ? null : asBoolean(value);
	}

	public static short asShort(Object value) {
		return (value instanceof Short) ? ((Short) value).shortValue() : Short.parseShort(asString(value));
	}

	public static Short asWrapperShort(Object value) {
		return null == value ? null : asShort(value);
	}

	public static int asInt(Object value) {
		if (null == value)
			return 0;
		return (value instanceof Number) ? ((Number) value).intValue() : Integer.parseInt(asString(value));
	}

	public static Integer asWrapperInteger(Object value) {
		return null == value ? null : asInt(value);
	}

	public static long asLong(Object value) {
		/** tq:如果不抛异常，会有隐患 */
		// (org.apache.commons.lang.math.NumberUtils.isNumber(stringValue))
		return (value instanceof Number) ? ((Number) value).longValue() : Long.parseLong(asString(value));
	}

	// @Deprecated //??
	public static long asLong(Object value, long nullValue) {
		return null == value ? nullValue : asLong(value);
	}

	public static Long asWrapperLong(Object value) {
		return null == value ? null : asLong(value);
	}

	public static double asDouble(Object value) {
		return (value instanceof Double) ? ((Double) value).doubleValue() : Double.parseDouble(asString(value));
	}

	public static Double asWrapperDouble(Object value) {
		return null == value ? null : asDouble(value);
	}

	public static String asString(Object object) {
		return (null == object) ? null : StringUtils.trim(String.valueOf(object));
	}

	public static String asString(Object value, String nullValue) {
		return null == value ? nullValue : asString(value);
	}

	/** asArray TODO:List<String> Exception */
	@SuppressWarnings("unchecked")
	public static <E> E[] asArray(List<E> list) {
		return notEmpty(list) ? (E[]) list.toArray() : null;
	}

	public static long[] asLongArray(List<Long> list) {
		Long[] array = list.toArray(new Long[0]);
		return notEmpty(list) ? ArrayUtils.toPrimitive(array) : null;
	}

	public static int[] asIntArray(List<Integer> list) {
		return notEmpty(list) ? ArrayUtils.toPrimitive((Integer[]) list.toArray()) : null;
	}

	/** asList */
	public static <T> List<T> asList(T[] array) {
		return Arrays.asList(array);
	}

	public static List<Long> asList(long[] array) {
		if (!notEmpty(array)) {
			return null;
		}
		return asList(ArrayUtils.toObject(array));
	}

	/** union */
	public static <E> List<E> union(@SuppressWarnings("unchecked") List<E>... lists) {
		List<E> result = new ArrayList<E>();
		if (null != lists) {
			for (List<E> list : lists) {
				if (notEmpty(list)) {
					result.addAll(list);
				}
			}
		}
		return (List<E>) stripEmpty(result);
	}

	public static <E> Set<E> union(@SuppressWarnings("unchecked") Set<E>... sets) {
		Set<E> result = new HashSet<E>();
		if (null != sets) {
			for (Set<E> set : sets) {
				if (notEmpty(set)) {
					result.addAll(set);
				}
			}
		}
		return stripEmpty(result);
	}

	public static <E> Collection<E> union(@SuppressWarnings("unchecked") Collection<E>... collections) {
		Collection<E> result = new ArrayList<E>();
		if (null != collections) {
			for (Collection<E> collection : collections) {
				if (notEmpty(collection)) {
					result.addAll(collection);
				}
			}
		}
		return stripEmpty(result);
	}

	public static long[] deduplicate(long[] values) {
		if (Detect.notEmpty(values)) {
			Set<Long> set = new LinkedHashSet<Long>();
			set.addAll(Arrays.asList(ArrayUtils.toObject(values)));
			return ArrayUtils.toPrimitive((Long[]) set.toArray(new Long[0]));
		}
		return null;
	}

	/** stripEmpty */
	public static <E> List<E> stripEmpty(List<E> list) {
		return notEmpty(list) ? list : null;
	}

	public static <E> Set<E> stripEmpty(Set<E> set) {
		return notEmpty(set) ? set : null;
	}

	public static <E> Collection<E> stripEmpty(Collection<E> collection) {
		return notEmpty(collection) ? collection : null;
	}

	// public static String stripVarchar(String value) {
	// return (notEmpty(value) && value.length() > 4000) ? value.substring(0,
	// 3990) : value;
	// }

	public static <E> E firstOne(E[] array) {
		return (null != array && array.length > 0) ? array[0] : null;
	}

	public static <E> E firstOne(List<E> list) {
		// return notEmpty(list) ? list.get(0) : null;
		return notEmpty(list) ? list.iterator().next() : null;
	}

	public static <E> E firstOne(Collection<E> collection) {
		// return notEmpty(list) ? list.get(0) : null;
		return notEmpty(collection) ? collection.iterator().next() : null;
	}

	// public static boolean onlyOne(List<?> list) {
	// return notEmpty(list) && list.size() == 1;
	// }

	public static boolean onlyOne(Collection<?> collection) {
		return notEmpty(collection) && collection.size() == 1;
	}

	public static boolean between(long value, long floor, long ceil) {
		return value >= floor && value <= ceil;
	}

	public static String trim(String string) {
		return StringUtils.trim(string);
	}

	public static String trimToNull(String string) {
		return StringUtils.trimToNull(string);
	}

	// /*
	// * 0 指前面补充零 formatLength 字符总长度为 formatLength d 代表为正数。
	// */
	// @Deprecated
	// /* use apache StringUtls.leftPad */
	// public static String frontCompWithZore(int source, int formatLength) {
	//
	// String newString = String.format("%0" + formatLength + "d", source);
	// return newString;
	// }

	/** endsWith */
	public static boolean endsWith(String target, String[] suffixs) {
		if (Detect.notEmpty(suffixs)) {
			for (String suffix : suffixs) {
				boolean result = target.endsWith(suffix);
				if (true == result) {
					return true;
				}
			}
		}
		return false;
	}

	public static String safeString(String value) {
		return (null == value) ? "" : value;
	}

	// 首字母转小写
	public static String lowerCaseFirstLetter(String value) {
		if (!notEmpty(value))
			return value;
		if (Character.isLowerCase(value.charAt(0)))
			return value;
		else
			return (new StringBuilder()).append(Character.toLowerCase(value.charAt(0))).append(value.substring(1)).toString();
	}

	// 首字母转大写
	public static String upperCaseFirstLetter(String value) {
		if (!notEmpty(value))
			return value;
		if (Character.isUpperCase(value.charAt(0)))
			return value;
		else
			return (new StringBuilder()).append(Character.toUpperCase(value.charAt(0))).append(value.substring(1)).toString();
	}
	//获取一个double的小数位数
	public static int getNumberByDouble(double number) {
		if (number == (long)number) {
			return 0;
		}
		int i = 0;
		while (true){
			i++;
			if (number * Math.pow(10, i) % 1 == 0) {
				return i;
			}
		}
	}
	public static int getNumberByStr(String number) {
//		String moneyStr = String.valueOf(number);
		String[] num = number.split("\\.");
		if (num.length == 2) {
			for (;;){
				if (num[1].endsWith("0")) {
					num[1] = num[1].substring(0, num[1].length() - 1);
				}else {
					break;
				}
			}
			return num[1].length();
		}else {
			return 0;
		}
	}
	/** 16进制 : 16 */
	private static final int HEX = 16;
	/** SHA: 0xFF  */
	private static final int SHA_FF = 0xFF;
	/** SHA: 0x100   */
	private static final int SHA_100 = 0x100;

	/**
	 * SHA算法实现
	 * @param msg 明文
	 * @param salt 盐
	 * @return 密文
	 */
	public static String encryptSHA(final String msg, String salt) {

		StringBuilder sb = new StringBuilder();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(salt.getBytes());
			byte[] bytes = md.digest(msg.getBytes());
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & SHA_FF) + SHA_100, HEX).substring(1));
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public static void main(String[] args) throws Exception {
		int numberByStr = getNumberByStr("5.6");
		System.out.println(numberByStr);
	}
}
