package com.ruoyi.ghxx.common;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitTemplateGet {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Value("${ruoyi.profile}")
    private String UPLOAD_PROFILE;
    public RabbitTemplate getRabbitTemplate(){
        return rabbitTemplate;
    }
    public String getProfile(){
        return UPLOAD_PROFILE;
    }
}
