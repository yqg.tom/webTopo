package com.ruoyi.ghxx.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.mapper.BEchartZtMapper;
import com.ruoyi.ghxx.domain.BEchartZt;
import com.ruoyi.ghxx.service.IBEchartZtService;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
/**
 * 大屏管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-24
 */
@Service
public class BEchartZtServiceImpl implements IBEchartZtService 
{
    @Autowired
    private BEchartZtMapper bEchartZtMapper;

    /**
     * 查询大屏管理
     * 
     * @param id 大屏管理主键
     * @return 大屏管理
     */
    @Override
    public BEchartZt selectBEchartZtById(Long id)
    {
        return bEchartZtMapper.selectBEchartZtById(id);
    }

    /**
     * 查询大屏管理列表
     * 
     * @param bEchartZt 大屏管理
     * @return 大屏管理
     */
    @Override
    public List<BEchartZt> selectBEchartZtList(BEchartZt bEchartZt)
    {
        return bEchartZtMapper.selectBEchartZtList(bEchartZt);
    }

    /**
     * 新增大屏管理
     * 
     * @param bEchartZt 大屏管理
     * @return 结果
     */
    @Override
    public int insertBEchartZt(BEchartZt bEchartZt)
    {
        Snowflake snowflake = IdUtil.getSnowflake(6, 2);
        long id = snowflake.nextId();
        bEchartZt.setId(id);
        bEchartZt.setCreateTime(DateUtils.getNowDate());
        return bEchartZtMapper.insertBEchartZt(bEchartZt);
    }

    /**
     * 修改大屏管理
     * 
     * @param bEchartZt 大屏管理
     * @return 结果
     */
    @Override
    public int updateBEchartZt(BEchartZt bEchartZt)
    {
        bEchartZt.setUpdateTime(DateUtils.getNowDate());
        return bEchartZtMapper.updateBEchartZt(bEchartZt);
    }

    /**
     * 批量删除大屏管理
     * 
     * @param ids 需要删除的大屏管理主键
     * @return 结果
     */
    @Override
    public int deleteBEchartZtByIds(Long[] ids)
    {
        return bEchartZtMapper.deleteBEchartZtByIds(ids);
    }

    /**
     * 删除大屏管理信息
     * 
     * @param id 大屏管理主键
     * @return 结果
     */
    @Override
    public int deleteBEchartZtById(Long id)
    {
        return bEchartZtMapper.deleteBEchartZtById(id);
    }
}
