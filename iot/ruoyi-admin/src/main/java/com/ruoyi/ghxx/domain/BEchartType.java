package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 图大全对象 b_echart_type
 * 
 * @author ruoyi
 * @date 2023-05-25
 */
@Data
public class BEchartType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** guid */
    private String guid;

    /** 部门权限 */
    private String deptIdStrs;
    /** 图表名称 */
    @Excel(name = "图表名称")
    private String echartName;
    /** 图表类别 */
    @Excel(name = "图表类别")
    private String echartType;

    /** 图表内容 */
    @Excel(name = "图表内容")
    private String echartData;

    /** 图表图片 */
    private String echartImgae;

    private String base64;
}
