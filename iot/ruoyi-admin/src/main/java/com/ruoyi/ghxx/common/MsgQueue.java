package com.ruoyi.ghxx.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MsgQueue implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 上报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date reporteTime;

    /** 主题 */
    private String topic;

    /** 服务质量 */
    private Integer qos;

    /** 消息内容 */
    private String message;

    private String imei;

    private String commandType;
}
