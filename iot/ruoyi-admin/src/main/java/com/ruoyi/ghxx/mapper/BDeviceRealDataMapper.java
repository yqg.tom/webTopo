package com.ruoyi.ghxx.mapper;

import java.util.List;
import com.ruoyi.ghxx.domain.BDeviceRealData;
import org.springframework.stereotype.Service;

/**
 * 设备实时数据Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-27
 */
@Service
public interface BDeviceRealDataMapper 
{
    /**
     * 查询设备实时数据
     * 
     * @param id 设备实时数据ID
     * @return 设备实时数据
     */
    public BDeviceRealData selectBDeviceRealDataById(Long id);

    /**
     * 查询设备实时数据列表
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 设备实时数据集合
     */
    public List<BDeviceRealData> selectBDeviceRealDataList(BDeviceRealData bDeviceRealData);

    /**
     * 新增设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    public int insertBDeviceRealData(BDeviceRealData bDeviceRealData);

    /**
     * 修改设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    public int updateBDeviceRealData(BDeviceRealData bDeviceRealData);

    /**
     * 删除设备实时数据
     * 
     * @param id 设备实时数据ID
     * @return 结果
     */
    public int deleteBDeviceRealDataById(Long id);

    /**
     * 批量删除设备实时数据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBDeviceRealDataByIds(Long[] ids);
}
