package com.ruoyi.ghxx.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class JsonToHex {
    public static void main(String[] args) throws Exception {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","张三");
        jsonObject.put("age",20);
        byte[] bytes = JSON.toJSONBytes(jsonObject);
        String json=new String(bytes,"UTF-8");
        System.out.println(json);
    }
}
