package com.ruoyi.ghxx.mapper;

import java.util.List;
import com.ruoyi.ghxx.domain.SysUpload;

/**
 * 上传信息Mapper接口
 * 
 * @author ruoyi
 * @date 2020-11-17
 */
public interface SysUploadMapper 
{
    /**
     * 查询上传信息
     * 
     * @param id 上传信息ID
     * @return 上传信息
     */
    public SysUpload selectSysUploadById(Long id);

    /**
     * 查询上传信息列表
     * 
     * @param sysUpload 上传信息
     * @return 上传信息集合
     */
    public List<SysUpload> selectSysUploadList(SysUpload sysUpload);

    /**
     * 新增上传信息
     * 
     * @param sysUpload 上传信息
     * @return 结果
     */
    public int insertSysUpload(SysUpload sysUpload);

    /**
     * 修改上传信息
     * 
     * @param sysUpload 上传信息
     * @return 结果
     */
    public int updateSysUpload(SysUpload sysUpload);

    /**
     * 删除上传信息
     * 
     * @param id 上传信息ID
     * @return 结果
     */
    public int deleteSysUploadById(Long id);

    /**
     * 批量删除上传信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysUploadByIds(Long[] ids);
}
