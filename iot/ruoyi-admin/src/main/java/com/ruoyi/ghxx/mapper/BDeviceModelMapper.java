package com.ruoyi.ghxx.mapper;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.domain.BDeviceModel;

/**
 * 三维配置Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Service
public interface BDeviceModelMapper 
{
    /**
     * 查询三维配置
     * 
     * @param id 三维配置主键
     * @return 三维配置
     */
    public BDeviceModel selectBDeviceModelById(Long id);

    /**
     * 查询三维配置列表
     * 
     * @param bDeviceModel 三维配置
     * @return 三维配置集合
     */
    public List<BDeviceModel> selectBDeviceModelList(BDeviceModel bDeviceModel);

    /**
     * 新增三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    public int insertBDeviceModel(BDeviceModel bDeviceModel);

    /**
     * 修改三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    public int updateBDeviceModel(BDeviceModel bDeviceModel);

    /**
     * 删除三维配置
     * 
     * @param id 三维配置主键
     * @return 结果
     */
    public int deleteBDeviceModelById(Long id);

    /**
     * 批量删除三维配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBDeviceModelByIds(Long[] ids);
}
