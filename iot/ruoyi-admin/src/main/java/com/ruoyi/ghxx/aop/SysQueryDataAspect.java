package com.ruoyi.ghxx.aop;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysDeptService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * 公共字段切面
 * @author cl
 * @date 2020-11-20
 */
@Aspect  // 使用@Aspect注解声明一个切面
@Component
public class SysQueryDataAspect {
    @Autowired
    private ISysDeptService deptService;
    /**
     * 这里我们使用注解的形式
     * 当然，我们也可以通过切点表达式直接指定需要拦截的package,需要拦截的class 以及 method
     * 切点表达式:   execution(...)
     */
    @Pointcut("@annotation(com.ruoyi.ghxx.aop.SysQueryData)")
    public void logPointCut() {}

    /**
     * 环绕通知 @Around  ， 当然也可以使用 @Before (前置通知)  @After (后置通知)
     * @param point
     * @return
     * @throws Throwable
     */
    @Before("logPointCut()")
    public Object around(JoinPoint point) throws Throwable {
        Object result = point.getArgs();
        try {
            queryData(point);
        } catch (Exception e) {
        }
        return result;
    }

    /**
     * 根据部门id权限查询
     * @param joinPoint
     */
    private Object queryData(JoinPoint joinPoint) throws  Throwable{
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SysQueryData sysQueryData = method.getAnnotation(SysQueryData.class);
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(sysQueryData != null){
            //请求的参数
            Object[] args = joinPoint.getArgs();
            Object object = args[0];
            if (object != null) {
                Class objectClass = object.getClass().getSuperclass();
                Field[] fs = objectClass.getDeclaredFields();
                for(Field f : fs) {
                    if("deptIdStrs".equals(f.getName())){
                        try{

                            Method getOrgIdMethod=objectClass.getDeclaredMethod("getDeptIdStrs");
                            Object o=getOrgIdMethod.invoke(object);
//                            if(o==null) {
//
//                            }
                            Method setOrgIdMethod = objectClass.getDeclaredMethod("setDeptIdStrs", String.class);
                            if(!"admin".equals(loginUser.getUsername())){
                                String dept="";
                                if(loginUser.getUser().getDeptId()!=null){
                                    SysDept sysDept = deptService.selectDeptById(loginUser.getUser().getDeptId());
                                    if(sysDept.getIsDepartment()!=null&&sysDept.getIsDepartment()==1&&sysDept.getParentId()!=100){
                                        dept=loginUser.getUser().getDept().getParentId()+"";
                                    }else if(loginUser.getUser().getDept().getDeptId()==null){
                                        dept=UUID.randomUUID().toString();
                                    }else{
                                        dept=loginUser.getUser().getDept().getDeptId()+"";
                                    }
                                }
                                setOrgIdMethod.invoke(object, dept+"");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return joinPoint.getArgs();
    }
}
