package com.ruoyi.ghxx.vo;

import lombok.Data;

@Data
public class BDeviceToken {
    private String token;
    private String ztGuid;
}
