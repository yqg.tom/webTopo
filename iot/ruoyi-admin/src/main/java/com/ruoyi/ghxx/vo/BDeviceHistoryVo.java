package com.ruoyi.ghxx.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 */
@Data
public class BDeviceHistoryVo implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private String imei;

    /** 模拟量1 */
    private Double a1;

    /** 模拟量2 */
    private Double a2;

    /** 模拟量3 */
    private Double a3;

    /** 模拟量4 */
    private Double a4;

    /** 模拟量5 */
    private Double a5;

    /** 模拟量6 */
    private Double a6;

    /** 模拟量7 */
    private Double a7;

    /** 模拟量 */
    private Double a8;

    /** 模拟量 */
    private Double a9;

    /** 模拟量 */
    
    private Double a10;

    /** 模拟量 */
    
    private Double a11;

    /** 模拟量 */
    
    private Double a12;

    /** 模拟量 */
    
    private Double a13;

    /** 模拟量1 */
    private Double a14;

    /** 模拟量2 */
    private Double a15;

    /** 模拟量3 */
    private Double a16;

    /** 模拟量4 */
    private Double a17;

    /** 模拟量5 */
    private Double a18;

    /** 模拟量6 */
    private Double a19;

    /** 模拟量7 */
    private Double a20;

    /** 模拟量 */
    
    private Double a21;

    /** 模拟量 */
    
    private Double a22;

    /** 模拟量 */
    
    private Double a23;

    /** 模拟量 */
    
    private Double a24;

    /** 控制量 */
    
    private Integer k1;

    /** 控制量 */
    
    private Integer k2;

    /** 控制量 */
    
    private Integer k3;

    /** 控制量 */
    
    private Integer k4;

    /** 控制量 */
    
    private Integer k5;

    /** 控制量 */
    
    private Integer k6;

    /** 控制量 */
    
    private Integer k7;

    /** 控制量 */
    
    private Integer k8;
    /** 控制量 */
    private Integer k9;
    /** 控制量 */
    private Integer k10;
    /** 控制量 */
    private Integer k11;
    /** 控制量 */
    private Integer k12;
    /** 控制量 */
    private Integer k13;
    /** 控制量 */
    private Integer k14;
    /** 控制量 */
    private Integer k15;
    /** 控制量 */
    private Integer k16;
    /** 控制量 */
    private Integer k17;
    /** 控制量 */
    private Integer k18;
    /** 控制量 */
    private Integer k19;
    /** 控制量 */
    private Integer k20;
    /** 控制量 */
    private Integer k21;
    /** 控制量 */
    private Integer k22;
    /** 控制量 */
    private Integer k23;
    /** 控制量 */
    private Integer k24;

    /** 控制量 */
    private String z1;

    /** 状态量 */
    private String z2;

    /** 状态量 */
    private String z3;

    /** 状态量 */
    private String z4;

    /** 状态量 */
    private String z5;

    /** 状态量 */
    private String z6;

    /** 状态量 */
    private String z7;

    /** 状态量 */
    private String z8;
    /** 状态量 */
    private String z9;
    /** 状态量 */
    private String z10;
    /** 状态量 */
    private String z11;
    /** 状态量 */
    private String z12;
    /** 状态量 */
    private String z13;
    /** 状态量 */
    private String z14;
    /** 状态量 */
    private String z15;
    /** 状态量 */
    private String z16;
    /** 状态量 */
    private String z17;
    /** 状态量 */
    private String z18;
    /** 状态量 */
    private String z19;
    /** 状态量 */
    private String z20;
    /** 状态量 */
    private String z21;
    /** 状态量 */
    private String z22;
    /** 状态量 */
    private String z23;
    /** 状态量 */
    private String z24;
    //远程升级和重启
    private Integer upgrade;
    private Integer restart;
}