package com.ruoyi.ghxx.service.impl;

import java.util.*;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.ghxx.domain.*;
import com.ruoyi.ghxx.util.Detect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.mapper.BDeviceRealDataMapper;
import com.ruoyi.ghxx.service.IBDeviceRealDataService;

/**
 * 设备实时数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-27
 */
@Service
public class BDeviceRealDataServiceImpl implements IBDeviceRealDataService 
{
    @Autowired
    private BDeviceRealDataMapper bDeviceRealDataMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private RedisCache redisCache;
    /**
     * 查询设备实时数据
     * 
     * @param id 设备实时数据ID
     * @return 设备实时数据
     */
    @Override
    public BDeviceRealData selectBDeviceRealDataById(Long id)
    {
        return bDeviceRealDataMapper.selectBDeviceRealDataById(id);
    }

    /**
     * 查询设备实时数据列表
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 设备实时数据
     */
    @Override
    public List<BDeviceRealData> selectBDeviceRealDataList(BDeviceRealData bDeviceRealData)
    {
        return bDeviceRealDataMapper.selectBDeviceRealDataList(bDeviceRealData);
    }

    /**
     * 新增设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    @Override
    public Integer insertBDeviceRealData(BDeviceRealData bDeviceRealData)
    {
        bDeviceRealData.setParamValue("");
        bDeviceRealData.setCreateTime(DateUtils.getNowDate());
        String guid =UUID.randomUUID().toString();
        bDeviceRealData.setGuid(guid);
        Snowflake snowflake = IdUtil.getSnowflake(6, 2);
        long id = snowflake.nextId();
        bDeviceRealData.setId(id);
        bDeviceRealDataMapper.insertBDeviceRealData(bDeviceRealData);
        return 200;
    }

    /**
     * 修改设备实时数据
     * 
     * @param bDeviceRealData 设备实时数据
     * @return 结果
     */
    @Override
    public Integer updateBDeviceRealData(BDeviceRealData bDeviceRealData)
    {
        bDeviceRealData.setUpdateTime(DateUtils.getNowDate());
        bDeviceRealDataMapper.updateBDeviceRealData(bDeviceRealData);
        return 200;
    }

    /**
     * 批量删除设备实时数据
     * 
     * @param ids 需要删除的设备实时数据ID
     * @return 结果
     */
    @Override
    public int deleteBDeviceRealDataByIds(Long[] ids)
    {
        List<BDeviceRealData> list=new ArrayList<>();
        for (int i = 0; i < ids.length; i++) {
            BDeviceRealData bDeviceRealData = bDeviceRealDataMapper.selectBDeviceRealDataById(ids[i]);
            list.add(bDeviceRealData);
        }
        bDeviceRealDataMapper.deleteBDeviceRealDataByIds(ids);
        return 200;
    }

    /**
     * 删除设备实时数据信息
     * 
     * @param id 设备实时数据ID
     * @return 结果
     */
    @Override
    public int deleteBDeviceRealDataById(Long id)
    {
        return bDeviceRealDataMapper.deleteBDeviceRealDataById(id);
    }

}
