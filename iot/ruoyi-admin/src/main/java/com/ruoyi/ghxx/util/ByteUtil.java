package com.ruoyi.ghxx.util;

import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ByteUtil {

    /**
     * Integer转Byte[]
     *
     * @param number
     * @return
     */
    public static byte[] intToByte(int number) {
        int temp = number;
        byte[] b = new byte[4];
        for (int i = 0; i < b.length; i++) {
            b[i] = new Integer(temp & 0xff).byteValue();
            temp = temp >> 8;
        }
        return b;
    }
    /**
     * Short转Byte[]
     *
     * @param number
     * @return
     */
    public static byte[] shortToByte(short number) {
        int temp = number;
        byte[] b = new byte[2];
        for (int i = 0; i < b.length; i++) {
            b[i] = new Integer(temp & 0xff).byteValue();
            temp = temp >> 8;
        }
        return b;
    }


    /**
     * Byte[]转Short
     *
     * @param shortByte
     * @return
     */
    public static int byteToShort(byte[] shortByte) {
        int fromByte = 0;
        for (int i = 0; i < 2; i++) {
            int n = (shortByte[i] < 0 ? (short) shortByte[i] + 256 : (short) shortByte[i]) << (8 * i);
            fromByte += n;
        }
        return fromByte;
    }

    /**
     * Double转Byte[]
     *
     * @param d
     * @return
     */
    public static byte[] doubleToByte(double d) {
        byte[] b = new byte[8];
        long l = Double.doubleToLongBits(d);
        for (int i = 0; i < b.length; i++) {
            b[i] = new Long(l).byteValue();
            l = l >> 8;
        }
        return b;
    }

    /**
     * Byte[]转Double
     *
     * @param b
     * @return
     */
    public static double byteToDouble(byte[] b) {
        long l;
        l = b[0];
        l &= 0xff;
        l |= ((long) b[1] << 8);
        l &= 0xffff;
        l |= ((long) b[2] << 16);
        l &= 0xffffff;
        l |= ((long) b[3] << 24);
        l &= 0xffffffffl;
        l |= ((long) b[4] << 32);
        l &= 0xffffffffffl;
        l |= ((long) b[5] << 40);
        l &= 0xffffffffffffl;
        l |= ((long) b[6] << 48);
        l &= 0xffffffffffffffl;
        l |= ((long) b[7] << 56);
        return Double.longBitsToDouble(l);
    }
    /**
     * 字节转换为浮点
     *
     * @param b 字节（至少4个字节）
     * @param index 开始位置
     * @return
     */
    public static float byte2float(byte[] b, int index) {
        int l;
        l = b[index + 0];
        l &= 0xff;
        l |= ((long) b[index + 1] << 8);
        l &= 0xffff;
        l |= ((long) b[index + 2] << 16);
        l &= 0xffffff;
        l |= ((long) b[index + 3] << 24);
        return Float.intBitsToFloat(l);
    }
    /**
     * Long转Byte[]
     *
     * @param num
     * @return
     */
    public static byte[] longToByte(long num) {
        byte[] b = new byte[8];
        long l = num;
        for (int i = 0; i < b.length; i++) {
            b[i] = new Long(l).byteValue();
            l = l >> 8;
        }
        return b;
    }

    /**
     * Byte[]转Long
     *
     * @param b
     * @return
     */
    public static long byteToLong(byte[] b) {
        long l;
        l = b[0];
        l &= 0xff;
        l |= ((long) b[1] << 8);
        l &= 0xffff;
        l |= ((long) b[2] << 16);
        l &= 0xffffff;
        l |= ((long) b[3] << 24);
        l &= 0xffffffffl;
        l |= ((long) b[4] << 32);
        l &= 0xffffffffffl;
        l |= ((long) b[5] << 40);
        l &= 0xffffffffffffl;
        l |= ((long) b[6] << 48);
        l &= 0xffffffffffffffl;
        l |= ((long) b[7] << 56);
        return l;
    }

    /**
     * Byte[]转String
     *
     * @param b
     * @param charsetName "GBK"
     * @return
     */
    public static String byteToString(byte[] b, String charsetName) {
        String str = null;
        try {
            if(StringUtils.isEmpty(charsetName)){
                charsetName = "GBK";
            }
            str = new String(b, charsetName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * Byte[]转String
     *
     * @param b
     * @return
     */
    public static String byteToString(byte[] b) {
        return byteToString(b, "GBK");
    }

    /**
     * String转Byte[]
     *
     * @param s
     * @return
     */
    public static byte[] stringToByte(String s) {
        byte[] bytes = stringToByte(s,"GBK");  //数据代理使用GBK编码方式获取值
        return bytes;
    }

    /**
     * String转Byte[]
     *
     * @param charsetName
     * @return
     */
    public static byte[] stringToByte(String s, String charsetName) {
        byte[] bytes = null;
        try {
            if(StringUtils.isEmpty(charsetName)){
                charsetName = "GBK";//数据代理使用GBK编码方式获取值
            }
            bytes = s.getBytes(charsetName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * Blob转成byte数组
     *
     * @param blob
     * @return
     */
    public static byte[] blobToBytes(Blob blob) {
        byte[] b = null;
        try {
            b = blob.getBytes(1L, (int) blob.length());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return b;
    }

    /**
     * Clob转成byte数组
     *
     * @param clob
     * @param charsetName
     * @return
     */
    public static byte[] clobToBytes(Clob clob, String charsetName) {
        byte[] b = null;
        try {
            if(clob==null) return null;
            String retVal = (clob != null ? clob.getSubString(1, (int) clob.length()) : null);
            if(StringUtils.isEmpty(retVal)){
                return null;
            }
            b = retVal.getBytes(StringUtils.isEmpty(charsetName)?"GBK":charsetName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    /**
     * Clob转成byte数组
     *
     * @param clob
     * @return
     */
    public static byte[] clobToBytes(Clob clob) {
        return clobToBytes(clob,"GBK") ;
    }
    /**
     * 字节数组转换为十六进制字符串
     *
     * @param bytData
     * @return
     */
    public static String byteToHexString(byte[] bytData, int start, int length) {
        StringBuilder sb = new StringBuilder();
        String hexStr="";
        for (int i = start; i < start + length; i++) {

            String hex = Integer.toHexString(bytData[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        hexStr=sb.toString().trim();
        return hexStr.replace("3","");
    }
    /**
     * 字节数组转换为十六进制字符串
     *
     * @param bytData
     * @return
     */
    public static String byteToHexString(byte[] bytData) {
        int t = 0;
        int start=0;
        int length=bytData.length;
        StringBuilder sb = new StringBuilder();
        String hexStr="";
        for (int i = start; i < start + length; i++) {

            String hex = Integer.toHexString(bytData[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(" ").append(hex.toUpperCase());

            t++;
        }
        hexStr=sb.toString().trim();
        return hexStr;
    }
    /**
     * 将指定的目标数组追加到原数组后面，返回一个新数组
     *
     * @param src  - 源数组。
     * @param dest - 目标数组。
     * @return
     */
    public static byte[] append(byte[] src, byte[] dest) {
        byte[] result = new byte[src.length + dest.length];
        System.arraycopy(src, 0, result, 0, src.length);
        System.arraycopy(dest, 0, result, src.length, dest.length);
        return result;
    }


    /**
     * 将指定的目标追加到原数组后面，返回一个新数组
     *
     * @param src  - 源数组。
     * @param dest - 目标。
     * @return
     */
    public static byte[] append(byte[] src, byte dest) {
        byte[] _result = new byte[src.length + 1];
        byte[] _dest = new byte[1];
        _dest[0] = dest;
        System.arraycopy(src, 0, _result, 0, src.length);
        System.arraycopy(_dest, 0, _result, src.length, _dest.length);
        return _result;
    }

    /**
     * 从输入流中读取到字节数组中
     * @param in           输入流
     * @param length       读取长度
     * @return
     * @throws IOException
     */
    public static byte[] readBytes(InputStream in, long length) throws IOException {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read = 0;
        while (read < length) {
            int cur = in.read(buffer, 0, (int) Math.min(1024, length - read));
            if (cur < 0) {
                break;
            }
            read += cur;
            bo.write(buffer, 0, cur);
        }
        return bo.toByteArray();
    }
    /**
     * 返回16进制的校验和（低八位）
     * @param
     * @param data //16进制字符串
     * @return
     */
    public static String makeCheckSum(String data){
        data=data.replaceAll(" ","").replaceAll("\n","");
        if(Detect.isEmpty(data)){
            return "";
        }
        int total=0;
        int len=data.length();
        int num = 0;
        while(num<len){
            String s = data.substring(num, num + 2);
            total += Integer.parseInt(s, 16);
            num = num + 2;
        }
        /**
         * 用256求余最大是255，即16进制的FF
         */
        int mod = total % 256;
        String hex = Integer.toHexString(mod);
        len = hex.length();
        // 如果不够校验位的长度，补0,这里用的是两位校验
        if (len < 2) {
            hex = "0" + hex;
        }
        return hex;
    }
    /**
     * 返回去除空格和转义字符\n的字符串
     * @param
     * @param str
     * @return
     */
    public static String getStrReplaceAll(String str){
        str=str.replaceAll(" ","").replaceAll("\n","");
        return str;
    }
    public static String AnalogCvert(byte[] byData, int iOffset) {
        short a = byte2short(byData, iOffset);
        return Short.valueOf(a).toString();
    }

    public static String AnalogCvertInt(byte[] byData, int iOffset) {
        int a = Byte2Int(byData, iOffset);
        return Integer.valueOf(a).toString();
    }
    static public int Byte2Int(byte[]bytes,int ii) {
        int sum = 0;
        int len =4;
        int end = ii + 4;
        for (int i = ii; i < end; i++) {
            int n = ((int)bytes[i]) & 0xff;
            n <<= (--len) * 8;
            sum += n;
        }
        return sum;

    }
    public static short byte2short(byte[] b,int offset){
        short l = 0;
        for (int i = 0; i < 2; i++) {
            l<<=8; //<<=和我们的 +=是一样的，意思就是 l = l << 8    0<<8 => 000000000000
            l |= (b[i+offset] & 0xff); //和上面也是一样的  l = l | (b[i]&0xff)
        }
        return l;
    }
    public static int getInt(byte[] bytes,int pos)
    {
        return (0xff & bytes[pos]) | (0xff00 & (bytes[pos+1] << 8)) | (0xff0000 & (bytes[pos+2] << 16)) | (0xff000000 & (bytes[pos+3] << 24));
    }

    static byte[] crc16_tab_h = { (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0,
            (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1,
            (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0,
            (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0,
            (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x01, (byte) 0xC0, (byte) 0x80, (byte) 0x41, (byte) 0x00, (byte) 0xC1, (byte) 0x81, (byte) 0x40 };

    static byte[] crc16_tab_l = { (byte) 0x00, (byte) 0xC0, (byte) 0xC1, (byte) 0x01, (byte) 0xC3, (byte) 0x03, (byte) 0x02, (byte) 0xC2, (byte) 0xC6, (byte) 0x06, (byte) 0x07, (byte) 0xC7, (byte) 0x05, (byte) 0xC5, (byte) 0xC4, (byte) 0x04, (byte) 0xCC, (byte) 0x0C, (byte) 0x0D, (byte) 0xCD, (byte) 0x0F, (byte) 0xCF, (byte) 0xCE, (byte) 0x0E, (byte) 0x0A, (byte) 0xCA, (byte) 0xCB, (byte) 0x0B, (byte) 0xC9, (byte) 0x09, (byte) 0x08, (byte) 0xC8, (byte) 0xD8, (byte) 0x18, (byte) 0x19, (byte) 0xD9, (byte) 0x1B, (byte) 0xDB, (byte) 0xDA, (byte) 0x1A, (byte) 0x1E, (byte) 0xDE, (byte) 0xDF, (byte) 0x1F, (byte) 0xDD, (byte) 0x1D, (byte) 0x1C, (byte) 0xDC, (byte) 0x14, (byte) 0xD4, (byte) 0xD5, (byte) 0x15, (byte) 0xD7, (byte) 0x17, (byte) 0x16, (byte) 0xD6, (byte) 0xD2, (byte) 0x12,
            (byte) 0x13, (byte) 0xD3, (byte) 0x11, (byte) 0xD1, (byte) 0xD0, (byte) 0x10, (byte) 0xF0, (byte) 0x30, (byte) 0x31, (byte) 0xF1, (byte) 0x33, (byte) 0xF3, (byte) 0xF2, (byte) 0x32, (byte) 0x36, (byte) 0xF6, (byte) 0xF7, (byte) 0x37, (byte) 0xF5, (byte) 0x35, (byte) 0x34, (byte) 0xF4, (byte) 0x3C, (byte) 0xFC, (byte) 0xFD, (byte) 0x3D, (byte) 0xFF, (byte) 0x3F, (byte) 0x3E, (byte) 0xFE, (byte) 0xFA, (byte) 0x3A, (byte) 0x3B, (byte) 0xFB, (byte) 0x39, (byte) 0xF9, (byte) 0xF8, (byte) 0x38, (byte) 0x28, (byte) 0xE8, (byte) 0xE9, (byte) 0x29, (byte) 0xEB, (byte) 0x2B, (byte) 0x2A, (byte) 0xEA, (byte) 0xEE, (byte) 0x2E, (byte) 0x2F, (byte) 0xEF, (byte) 0x2D, (byte) 0xED, (byte) 0xEC, (byte) 0x2C, (byte) 0xE4, (byte) 0x24, (byte) 0x25, (byte) 0xE5, (byte) 0x27, (byte) 0xE7,
            (byte) 0xE6, (byte) 0x26, (byte) 0x22, (byte) 0xE2, (byte) 0xE3, (byte) 0x23, (byte) 0xE1, (byte) 0x21, (byte) 0x20, (byte) 0xE0, (byte) 0xA0, (byte) 0x60, (byte) 0x61, (byte) 0xA1, (byte) 0x63, (byte) 0xA3, (byte) 0xA2, (byte) 0x62, (byte) 0x66, (byte) 0xA6, (byte) 0xA7, (byte) 0x67, (byte) 0xA5, (byte) 0x65, (byte) 0x64, (byte) 0xA4, (byte) 0x6C, (byte) 0xAC, (byte) 0xAD, (byte) 0x6D, (byte) 0xAF, (byte) 0x6F, (byte) 0x6E, (byte) 0xAE, (byte) 0xAA, (byte) 0x6A, (byte) 0x6B, (byte) 0xAB, (byte) 0x69, (byte) 0xA9, (byte) 0xA8, (byte) 0x68, (byte) 0x78, (byte) 0xB8, (byte) 0xB9, (byte) 0x79, (byte) 0xBB, (byte) 0x7B, (byte) 0x7A, (byte) 0xBA, (byte) 0xBE, (byte) 0x7E, (byte) 0x7F, (byte) 0xBF, (byte) 0x7D, (byte) 0xBD, (byte) 0xBC, (byte) 0x7C, (byte) 0xB4, (byte) 0x74,
            (byte) 0x75, (byte) 0xB5, (byte) 0x77, (byte) 0xB7, (byte) 0xB6, (byte) 0x76, (byte) 0x72, (byte) 0xB2, (byte) 0xB3, (byte) 0x73, (byte) 0xB1, (byte) 0x71, (byte) 0x70, (byte) 0xB0, (byte) 0x50, (byte) 0x90, (byte) 0x91, (byte) 0x51, (byte) 0x93, (byte) 0x53, (byte) 0x52, (byte) 0x92, (byte) 0x96, (byte) 0x56, (byte) 0x57, (byte) 0x97, (byte) 0x55, (byte) 0x95, (byte) 0x94, (byte) 0x54, (byte) 0x9C, (byte) 0x5C, (byte) 0x5D, (byte) 0x9D, (byte) 0x5F, (byte) 0x9F, (byte) 0x9E, (byte) 0x5E, (byte) 0x5A, (byte) 0x9A, (byte) 0x9B, (byte) 0x5B, (byte) 0x99, (byte) 0x59, (byte) 0x58, (byte) 0x98, (byte) 0x88, (byte) 0x48, (byte) 0x49, (byte) 0x89, (byte) 0x4B, (byte) 0x8B, (byte) 0x8A, (byte) 0x4A, (byte) 0x4E, (byte) 0x8E, (byte) 0x8F, (byte) 0x4F, (byte) 0x8D, (byte) 0x4D,
            (byte) 0x4C, (byte) 0x8C, (byte) 0x44, (byte) 0x84, (byte) 0x85, (byte) 0x45, (byte) 0x87, (byte) 0x47, (byte) 0x46, (byte) 0x86, (byte) 0x82, (byte) 0x42, (byte) 0x43, (byte) 0x83, (byte) 0x41, (byte) 0x81, (byte) 0x80, (byte) 0x40 };

    /**
     * 计算CRC16校验
     *
     * @param data
     *            需要计算的数组
     * @return CRC16校验值
     */
    public static int calcCrc16(byte[] data) {
        return calcCrc16(data, 0, data.length);
    }

    /**
     * 计算CRC16校验
     *
     * @param data
     *            需要计算的数组
     * @param offset
     *            起始位置
     * @param len
     *            长度
     * @return CRC16校验值
     */
    public static int calcCrc16(byte[] data, int offset, int len) {
        return calcCrc16(data, offset, len, 0xffff);
    }

    /**
     * 计算CRC16校验
     *
     * @param data
     *            需要计算的数组
     * @param offset
     *            起始位置
     * @param len
     *            长度
     * @param preval
     *            之前的校验值
     * @return CRC16校验值
     */
    public static int calcCrc16(byte[] data, int offset, int len, int preval) {
        int ucCRCHi = (preval & 0xff00) >> 8;
        int ucCRCLo = preval & 0x00ff;
        int iIndex;
        for (int i = 0; i < len; ++i) {
            iIndex = (ucCRCLo ^ data[offset + i]) & 0x00ff;
            ucCRCLo = ucCRCHi ^ crc16_tab_h[iIndex];
            ucCRCHi = crc16_tab_l[iIndex];
        }
        return ((ucCRCHi & 0x00ff) << 8) | (ucCRCLo & 0x00ff) & 0xffff;
    }
    /**
     * @description: BCC异或运算
     *         String str="CCTXA,0416686,1,1,303030313638303031433032433234373337324437423035363031383039363332343031364534453631343245384141314434323042303643343032433436373136";
     *         String bcc = getBCC(str.getBytes());
     *         System.out.println(bcc);--bcc=77
     */
    public static String getBCC(byte[] data){
        String ret = "";
        byte BCC[]= new byte[1];
        for(int i=0;i<data.length;i++)
        {
            BCC[0]=(byte) (BCC[0] ^ data[i]);
        }
        String hex = Integer.toHexString(BCC[0] & 0xFF);
        if (hex.length() == 1) {
            hex = '0' + hex;
        }
        ret += hex.toUpperCase();
        return ret;
    }
    /**
     * -----ASCII码转换为16进制 -----
     * 字符串: 68001001C247372D7B056018096324016EDD16
     * 转换为16进制 : 3638303031303031433234373337324437423035363031383039363332343031364544443136
     *
     */
    public String convertStringToHex(String str){

        char[] chars = str.toCharArray();

        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++){
            hex.append(Integer.toHexString((int)chars[i]));
        }

        return hex.toString();
    }
    /**
     * ***** 16进制转换为ASCII *****
     * Hex : 3638303031303031433234373337324437423035363031383039363332343031364544443136
     * ASCII : 000168001001C247372D7B056054F4C210016E5B5E42E2A5324209E79200000816
     *
     */
    public static  String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }

        return sb.toString();
    }
    /**
     * @description: 十进制转十六进制字符串，不够两位补零
     */
    public static String intToHexStr(int num){
        String hexStr="";
        if(num<10){
            hexStr="0"+Integer.toHexString(num);
        }else if(num>255&&num<=65535){
            hexStr=Integer.toHexString(num);
            if(hexStr.length()<4){
                hexStr="0"+hexStr;
            }
        }else if(num>65535){
            hexStr=Integer.toHexString(num);
            if(hexStr.length()<6){
                hexStr="0"+hexStr;
            }
        }else{
            hexStr=Integer.toHexString(num);
            if(hexStr.length()<2){
                hexStr="0"+hexStr;
            }
        }
        return hexStr;
    }
    /**
     * @param: [content]
     * @return: int
     * @description: 十六进制数组字符串转十进制
     */
    public static long hexStrCovertToLong(String hexStr){
        hexStr=hexStr.replace(" ","");
        long number=0;
        String [] HighLetter = {"A","B","C","D","E","F"};
        Map<String,Integer> map = new HashMap<>();
        for(int i = 0;i <= 9;i++){
            map.put(i+"",i);
        }
        for(int j= 10;j<HighLetter.length+10;j++){
            map.put(HighLetter[j-10],j);
        }
        String[]str = new String[hexStr.length()];
        for(int i = 0; i < str.length; i++){
            str[i] = hexStr.substring(i,i+1);
        }
        for(int i = 0; i < str.length; i++){
            number += map.get(str[i])*Math.pow(16,str.length-1-i);
        }
        return number;
    }
    /**
     * @param: bytes 数组 start 开始  offset偏移
     * @return: int
     * @description: byte数组转十进制
     */
    public static long hexStrCovertToLong(byte[] bytes, int start, int offset){
        long number=0;
        try {
            String string = ByteUtil.byteToHexString(bytes, start, offset);
            number = Long.parseLong(string,16);
        }catch (Exception e){
            //e.printStackTrace();
            System.out.println("数据解析出错，默认赋值为-255");
            number=-255;
        }
        return number;
    }
    /**
     * 十六进制字符串转成byte数组
     *
     * @param hexStr
     * @return
     */
    public static byte[] hexStrConverToBytes(String hexStr) {
        hexStr=hexStr.replace(" ","");
        Integer len = hexStr.length() / 2;
        byte[] bytes = new byte[len];
        for (int i = 0; i < len; i++) {
            byte out = (byte) Long.parseLong(hexStr.substring(0, 2), 16);
            bytes[i] = out;
            hexStr = hexStr.substring(2);
        }
        return bytes;
    }
    /*
    * 大端转换成小端
    * */
    public static String bigEndToSmallEnd(String str){
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        byte temporary;
        for (int i = 0; i < bytes.length/2; i+=2) {
            temporary=bytes[bytes.length-i-2];
            bytes[bytes.length-i-2]=bytes[i];
            bytes[i]=temporary;

            temporary=bytes[bytes.length-i-1];
            bytes[bytes.length-i-1]=bytes[i+1];
            bytes[i+1]=temporary;
        }
        return new String(bytes);
    }
    public static void main(String[] args) {
        String byteStr="5F F5 02 38 36 38 36 32 36 30 34 34 32 34 39 32 37 37 00 00 03 00 01 00 75 0D 0A ";
        byte[] bytes = hexStrConverToBytes(byteStr);
        byte[] bytes1=new byte[3];
        bytes1[0]=0x0A;
        bytes1[1]=0x0B;
        bytes1[2]=0x0C;
        byte[] append = append(bytes, bytes1);
        String hex=byteToHexString(append,0,append.length);
        System.out.println(hex);
        byte[] byte2=new byte[5];
        byte2[0]=0x5F;
        byte2[1]=0x01;
        byte2[2]=0x01;
        byte2[3]=0x01;
        byte2[4]=0x01;

        String s1 = AnalogCvert(byte2, 1);
        String s2 = AnalogCvertInt(byte2, 1);
        System.out.println( String.valueOf(byte2[0]));
        System.out.println(s1);
        System.out.println(s2);

        String string = convertHexToString("AAAA2800822105191150014502D9A7110000000000000002120000000000000000433124313500003600000D0A");
        System.out.println();
        System.out.println(string);
    }
}


