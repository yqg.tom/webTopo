package com.ruoyi.ghxx.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备历史记录对象 b_device_history
 *
 * @author ruoyi
 * @date 2021-11-25
 */
@Data
public class BDeviceHistoryRunState implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "上传时间",dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 设备号 */
    @Excel(name = "设备号")
    private String deviceMac;
    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 风机运行时长 */
    @Excel(name = "风机运行时长(天)")
    private String fanRunTime ;
    /** 空调运行时长 */
    @Excel(name = "空调运行时长(天)")
    private String airRunTime;
    /** 清洁次数 */
    @Excel(name = "清洁次数(次)")
    private String cleanNum ;
}