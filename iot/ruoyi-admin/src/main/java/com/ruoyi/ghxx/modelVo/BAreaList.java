package com.ruoyi.ghxx.modelVo;

import lombok.Data;

import java.util.List;

@Data
public class BAreaList {
    private List<BArea> provinceList;
    private List<BArea> cityList;
    private List<BArea> districtList;

}
