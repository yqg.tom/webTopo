package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 自定义组件对象 b_vue_component
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
@Data
public class BVueComponent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /** 组件名称 */
    @Excel(name = "组件名称")
    private String componentName;

    /** 组件图片 */
    private String componentImage;

    /** 组件模板 */
    private String componentTemplate;

    /** 组件样式 */
    private String componentStyle;

    /** 组件脚本 */
    private String componentScript;

    /** 组件外链 */
    private String componentExternalLink;
    /** 是否分享 */
    private Integer isShare;
    private String base64;
}
