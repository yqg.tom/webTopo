package com.ruoyi.ghxx.mapper;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.domain.BEchartType;

/**
 * 图大全Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-25
 */
@Service
public interface BEchartTypeMapper 
{
    /**
     * 查询图大全
     * 
     * @param id 图大全主键
     * @return 图大全
     */
    public BEchartType selectBEchartTypeById(Long id);

    /**
     * 查询图大全列表
     * 
     * @param bEchartType 图大全
     * @return 图大全集合
     */
    public List<BEchartType> selectBEchartTypeList(BEchartType bEchartType);

    /**
     * 新增图大全
     * 
     * @param bEchartType 图大全
     * @return 结果
     */
    public int insertBEchartType(BEchartType bEchartType);

    /**
     * 修改图大全
     * 
     * @param bEchartType 图大全
     * @return 结果
     */
    public int updateBEchartType(BEchartType bEchartType);

    /**
     * 删除图大全
     * 
     * @param id 图大全主键
     * @return 结果
     */
    public int deleteBEchartTypeById(Long id);

    /**
     * 批量删除图大全
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBEchartTypeByIds(Long[] ids);
}
