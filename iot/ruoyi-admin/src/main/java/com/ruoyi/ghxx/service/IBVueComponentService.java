package com.ruoyi.ghxx.service;

import java.util.List;
import com.ruoyi.ghxx.domain.BVueComponent;

/**
 * 自定义组件Service接口
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
public interface IBVueComponentService 
{
    /**
     * 查询自定义组件
     * 
     * @param id 自定义组件主键
     * @return 自定义组件
     */
    public BVueComponent selectBVueComponentById(Long id);

    /**
     * 查询自定义组件列表
     * 
     * @param bVueComponent 自定义组件
     * @return 自定义组件集合
     */
    public List<BVueComponent> selectBVueComponentList(BVueComponent bVueComponent);

    /**
     * 新增自定义组件
     * 
     * @param bVueComponent 自定义组件
     * @return 结果
     */
    public int insertBVueComponent(BVueComponent bVueComponent);

    /**
     * 修改自定义组件
     * 
     * @param bVueComponent 自定义组件
     * @return 结果
     */
    public int updateBVueComponent(BVueComponent bVueComponent);

    /**
     * 批量删除自定义组件
     * 
     * @param ids 需要删除的自定义组件主键集合
     * @return 结果
     */
    public int deleteBVueComponentByIds(Long[] ids);

    /**
     * 删除自定义组件信息
     * 
     * @param id 自定义组件主键
     * @return 结果
     */
    public int deleteBVueComponentById(Long id);
}
