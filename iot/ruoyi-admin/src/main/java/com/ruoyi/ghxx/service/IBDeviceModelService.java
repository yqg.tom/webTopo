package com.ruoyi.ghxx.service;

import java.util.List;
import com.ruoyi.ghxx.domain.BDeviceModel;

/**
 * 三维配置Service接口
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public interface IBDeviceModelService 
{
    /**
     * 查询三维配置
     * 
     * @param id 三维配置主键
     * @return 三维配置
     */
    public BDeviceModel selectBDeviceModelById(Long id);

    /**
     * 查询三维配置列表
     * 
     * @param bDeviceModel 三维配置
     * @return 三维配置集合
     */
    public List<BDeviceModel> selectBDeviceModelList(BDeviceModel bDeviceModel);

    /**
     * 新增三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    public int insertBDeviceModel(BDeviceModel bDeviceModel);

    /**
     * 修改三维配置
     * 
     * @param bDeviceModel 三维配置
     * @return 结果
     */
    public int updateBDeviceModel(BDeviceModel bDeviceModel);

    /**
     * 批量删除三维配置
     * 
     * @param ids 需要删除的三维配置主键集合
     * @return 结果
     */
    public int deleteBDeviceModelByIds(Long[] ids);

    /**
     * 删除三维配置信息
     * 
     * @param id 三维配置主键
     * @return 结果
     */
    public int deleteBDeviceModelById(Long id);
}
