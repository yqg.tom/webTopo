package com.ruoyi.ghxx.controller;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.ghxx.aop.SysInitData;
import com.ruoyi.ghxx.aop.SysInitUpdateData;
import com.ruoyi.ghxx.aop.SysQueryData;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.ghxx.domain.BDeviceModel;
import com.ruoyi.ghxx.service.IBDeviceModelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 三维配置Controller
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@RestController
@RequestMapping("/ghxx/bDeviceModel")
public class BDeviceModelController extends BaseController
{
    @Autowired
    private IBDeviceModelService bDeviceModelService;

    /**
     * 查询三维配置列表
     */
//    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:list')")
    @GetMapping("/list")
    @SysQueryData
    public TableDataInfo list(BDeviceModel bDeviceModel)
    {
        startPage();
        if(!SecurityUtils.getUsername().equals("admin")){
            bDeviceModel.setUserName(SecurityUtils.getUsername());
            bDeviceModel.setStatus(1);
        }
        List<BDeviceModel> list = bDeviceModelService.selectBDeviceModelList(bDeviceModel);
        return getDataTable(list);
    }

    /**
     * 导出三维配置列表
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:export')")
    @Log(title = "三维配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BDeviceModel bDeviceModel)
    {
        List<BDeviceModel> list = bDeviceModelService.selectBDeviceModelList(bDeviceModel);
        ExcelUtil<BDeviceModel> util = new ExcelUtil<BDeviceModel>(BDeviceModel.class);
        util.exportExcel(response, list, "三维配置数据");
    }

    /**
     * 获取三维配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(bDeviceModelService.selectBDeviceModelById(id));
    }

    /**
     * 新增三维配置
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:add')")
    @Log(title = "三维配置", businessType = BusinessType.INSERT)
    @PostMapping
    @SysInitData
    public AjaxResult add(@RequestBody BDeviceModel bDeviceModel)
    {
        return toAjax(bDeviceModelService.insertBDeviceModel(bDeviceModel));
    }

    /**
     * 修改三维配置
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:edit')")
    @Log(title = "三维配置", businessType = BusinessType.UPDATE)
    @PutMapping
    @SysInitUpdateData
    public AjaxResult edit(@RequestBody BDeviceModel bDeviceModel)
    {
        return toAjax(bDeviceModelService.updateBDeviceModel(bDeviceModel));
    }

    /**
     * 删除三维配置
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bDeviceModel:remove')")
    @Log(title = "三维配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bDeviceModelService.deleteBDeviceModelByIds(ids));
    }
}
