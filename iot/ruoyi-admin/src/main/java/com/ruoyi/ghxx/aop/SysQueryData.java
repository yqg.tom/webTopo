package com.ruoyi.ghxx.aop;

import java.lang.annotation.*;

/**
 * 定义查询字段初始化注解，根据部门id查询
 * @author cl
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysQueryData {
    String value() default "";
}
