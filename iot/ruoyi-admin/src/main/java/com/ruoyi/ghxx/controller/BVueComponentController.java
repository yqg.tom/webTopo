package com.ruoyi.ghxx.controller;

import java.io.File;
import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.ghxx.aop.SysInitData;
import com.ruoyi.ghxx.aop.SysInitUpdateData;
import com.ruoyi.ghxx.aop.SysQueryData;
import com.ruoyi.ghxx.domain.BEchartType;
import com.ruoyi.ghxx.util.Detect;
import com.ruoyi.ghxx.util.ImageUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.ghxx.domain.BVueComponent;
import com.ruoyi.ghxx.service.IBVueComponentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 自定义组件Controller
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
@RestController
@RequestMapping("/ghxx/bVueComponent")
public class BVueComponentController extends BaseController
{
    @Autowired
    private IBVueComponentService bVueComponentService;

    /**
     * 查询自定义组件列表
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:list')")
    @GetMapping("/list")
    @SysQueryData
    public TableDataInfo list(BVueComponent bVueComponent)
    {
        startPage();
        List<BVueComponent> list = bVueComponentService.selectBVueComponentList(bVueComponent);
        return getDataTable(list);
    }

    /**
     * 导出自定义组件列表
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:export')")
    @Log(title = "自定义组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BVueComponent bVueComponent)
    {
        List<BVueComponent> list = bVueComponentService.selectBVueComponentList(bVueComponent);
        ExcelUtil<BVueComponent> util = new ExcelUtil<BVueComponent>(BVueComponent.class);
        util.exportExcel(response, list, "自定义组件数据");
    }

    /**
     * 获取自定义组件详细信息
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(bVueComponentService.selectBVueComponentById(id));
    }

    /**
     * 新增自定义组件
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:add')")
    @Log(title = "自定义组件", businessType = BusinessType.INSERT)
    @PostMapping
    @SysInitData
    public AjaxResult add(@RequestBody BVueComponent bVueComponent)
    {
        bVueComponent.setComponentTemplate("<div id=\"app\" class=\"h2-text\">\n" +
                "    <h2>这是一个自定义组件</h2>\n" +
                "     <el-button type=\"primary\" @click=\"handleClick\">点击我</el-button>\n" +
                "</div>");
        bVueComponent.setComponentStyle(".h2-text {\n" +
                "    color:#67C23A\n" +
                "}");
        bVueComponent.setComponentScript("export default {\n" +
                "    data() {\n" +
                "        return {}\n" +
                "    },\n" +
                "    created() {\n" +
                "\n" +
                "    },\n" +
                "    mounted(){\n" +
                "\n" +
                "    },\n" +
                "    methods:{\n" +
                "        handleClick(){\n" +
                "             this.$message('这是一条消息提示');\n" +
                "        }\n" +
                "    }\n" +
                "}");
//        bVueComponent.setComponentExternalLink("<script src=\"https://cdn.bootcss.com/vue/2.6.8/vue.min.js\"></script>\n" +
//                "<link rel=\"stylesheet\" href=\"https://unpkg.com/element-ui/lib/theme-chalk/index.css\">\n" +
//                "<script src=\"https://unpkg.com/element-ui/lib/index.js\"></script>\n" +
//                "<script src=\"https://unpkg.com/axios/dist/axios.min.js\"></script>");
        return toAjax(bVueComponentService.insertBVueComponent(bVueComponent));
    }

    /**
     * 修改自定义组件
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:edit')")
    @Log(title = "自定义组件", businessType = BusinessType.UPDATE)
    @PutMapping
    @SysInitUpdateData
    public AjaxResult edit(@RequestBody BVueComponent bVueComponent)
    {
        return toAjax(bVueComponentService.updateBVueComponent(bVueComponent));
    }

    /**
     * 删除自定义组件
     */
    @PreAuthorize("@ss.hasPermi('ghxx:bVueComponent:remove')")
    @Log(title = "自定义组件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bVueComponentService.deleteBVueComponentByIds(ids));
    }
    /*
     * 保存组态缩略图
     * */
    @PostMapping(value = "/saveComponentImage")
    public AjaxResult saveComponentImage(@RequestBody BVueComponent bVueComponent)
    {
        MultipartFile image =ImageUtils.base64ToMultipartFile(bVueComponent.getBase64());
        String imageUrl="";
        try{
            imageUrl = FileUploadUtils.upload(RuoYiConfig.getUploadPath(), image);
        }catch (Exception e){
            return AjaxResult.error(500,"上传组件异常，"+e.toString());
        }
        if(Detect.notEmpty(bVueComponent.getComponentImage())){
            String newUrl = bVueComponent.getComponentImage().replace("/profile", "").replace("/avatar", "");
            File fileQuery=new File(RuoYiConfig.getAvatarPath()+newUrl);
            if(fileQuery.exists()){
                System.out.println("删除之前的图片资源");
                fileQuery.delete();
            }
        }
        bVueComponent.setComponentImage(imageUrl);
        bVueComponentService.updateBVueComponent(bVueComponent);
        return AjaxResult.success();
    }
}
