package com.ruoyi.ghxx.util;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtilLocal extends DateUtils {
	
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";
	public static final String TIMESTAMP_PATTERN_MM = "yyyy-MM-dd HH:mm";
	public static final String TIME_PATTERN = "HH:mm";
	/**
	 * 精确到毫秒的完整时间    如：yyyy-MM-dd HH:mm:ss.S
	 */
	public static String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.S";
	/**
	 * 中文简写  如：2010年12月01日
	 */
	public static String FORMAT_SHORT_CN = "yyyy年MM月dd";

	/**
	 * 中文全称  如：2010年12月01日  23时15分06秒
	 */
	public static String FORMAT_LONG_CN = "yyyy年MM月dd日  HH时mm分ss秒";

	/**
	 * 精确到毫秒的完整中文时间
	 */
	public static String FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒";

	public static Date now() {
		return new Date();
	}

	/**
	 * 解析日期<br>
	 * 支持格式：<br>
	 * generate by: vakin jiang at 2012-3-1
	 *
	 * @param dateStr
	 * @return
	 */
	public static Date parseDate(String dateStr) {
		SimpleDateFormat format = null;
		if (StringUtils.isBlank(dateStr))
			return null;
		String _dateStr = dateStr.trim();
		try {
			if (_dateStr.matches("\\d{1,2}[A-Z]{3}")) {
				_dateStr = _dateStr + (Calendar.getInstance().get(Calendar.YEAR) - 2000);
			}
			// 01OCT12
			if (_dateStr.matches("\\d{1,2}[A-Z]{3}\\d{2}")) {
				format = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
			} else if (_dateStr.matches("\\d{1,2}[A-Z]{3}\\d{4}.*")) {// 01OCT2012
				// ,01OCT2012
				// 1224,01OCT2012
				// 12:24
				_dateStr = _dateStr.replaceAll("[^0-9A-Z]", "").concat("000000").substring(0, 15);
				format = new SimpleDateFormat("ddMMMyyyyHHmmss", Locale.ENGLISH);
			} else {
				StringBuffer sb = new StringBuffer(_dateStr);
				String[] tempArr = _dateStr.split("\\s+");
				tempArr = tempArr[0].split("-|\\/");
				if (tempArr.length == 3) {
					if (tempArr[1].length() == 1) {
						sb.insert(5, "0");
					}
					if (tempArr[2].length() == 1) {
						sb.insert(8, "0");
					}
				}
				_dateStr = sb.append("000000").toString().replaceAll("[^0-9]", "").substring(0, 14);
				if (_dateStr.matches("\\d{14}")) {
					format = new SimpleDateFormat("yyyyMMddHHmmss");
				}
			}
			Date date = format.parse(_dateStr);
			return date;
		} catch (Exception e) {
			throw new RuntimeException("无法解析日期字符[" + dateStr + "]");
		}
	}

	/**
	 * 解析日期字符串转化成日期格式<br>
	 * generate by: vakin jiang at 2012-3-1
	 * 
	 * @param dateStr
	 * @param pattern
	 * @return
	 */
	public static Date parseDate(String dateStr, String pattern) {
		try {
			SimpleDateFormat format = null;
			if (StringUtils.isBlank(dateStr))
				return null;
			if (StringUtils.isNotBlank(pattern)) {
				format = new SimpleDateFormat(pattern);
				return format.parse(dateStr);
			}
			return parseDate(dateStr);
		} catch (Exception e) {
			throw new RuntimeException("无法解析日期字符[" + dateStr + "]");
		}
	}

	/**
	 * 获取一天开始时间<br>
	 * generate by: vakin jiang at 2011-12-23
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDayBegin(Date date) {
		String format = DateFormatUtils.format(date, DATE_PATTERN);
		return parseDate(format.concat(" 00:00:00"));
	}

	/**
	 * 获取一天结束时间<br>
	 * generate by: vakin jiang at 2011-12-23
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDayEnd(Date date) {
		String format = DateFormatUtils.format(date, DATE_PATTERN);
		return parseDate(format.concat(" 23:59:59"));
	}

	/**
	 * 时间戳格式转换为日期（年月日）格式<br>
	 * generate by: vakin jiang at 2011-12-23
	 * 
	 * @param date
	 * @return
	 */
	public static Date timestamp2Date(Date date) {
		return formatDate(date, DATE_PATTERN);
	}

	/**
	 * 格式化日期格式为：ddMMMyy<br>
	 * generate by: vakin jiang at 2012-10-17
	 * 
	 * @param
	 * @return
	 */
	public static String format2ddMMMyy(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
		return format.format(date).toUpperCase();
	}

	/**
	 * 格式化日期格式为：ddMMMyy<br>
	 * generate by: vakin jiang at 2012-10-17
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String format2ddMMMyy(String dateStr) {
		SimpleDateFormat format = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
		return format.format(DateUtilLocal.parseDate(dateStr)).toUpperCase();
	}

	/**
	 * 格式化日期字符串<br>
	 * generate by: vakin jiang at 2012-3-7
	 * 
	 * @param dateStr
	 * @param patterns
	 * @return
	 */
	public static String formatDateStr(String dateStr, String... patterns) {
		String pattern = TIMESTAMP_PATTERN;
		return DateUtilLocal.format(parseDate(dateStr,pattern), pattern);
	}

	/**
	 * 格式化日期为日期字符串<br>
	 * generate by: vakin jiang at 2012-3-7
	 * 
	 * @param
	 * @param patterns
	 * @return
	 */
	public static String format(Date date, String... patterns) {
		if (date == null)
			return "";
		String pattern = TIMESTAMP_PATTERN;
		if (patterns != null && patterns.length > 0 && StringUtils.isNotBlank(patterns[0])) {
			pattern = patterns[0];
		}
		return DateFormatUtils.format(date, pattern);
	}

	public static String format2DateStr(Date date) {
		return format(date, DATE_PATTERN);
	}

	/**
	 * 格式化日期为指定格式<br>
	 * generate by: vakin jiang at 2012-3-7
	 * 
	 * @param orig
	 * @param patterns
	 * @return
	 */
	public static Date formatDate(Date orig, String... patterns) {
		String pattern = TIMESTAMP_PATTERN;
		if (patterns != null && patterns.length > 0 && StringUtils.isNotBlank(patterns[0])) {
			pattern = patterns[0];
		}
		return parseDate(DateFormatUtils.format(orig, pattern));
	}

	/**
	 * 比较两个时间相差多少秒
	 * */
	public static long getDiffSeconds(Date d1, Date d2) {
		return Math.abs((d2.getTime() - d1.getTime()) / 1000);
	}

	/**
	 * 比较两个时间相差多少分钟
	 * */
	public static long getDiffMinutes(Date d1, Date d2) {
		long diffSeconds = getDiffSeconds(d1, d2);
		return diffSeconds / 60;
	}

	/**
	 * 比较两个时间相差多少天
	 * */
	public static long getDiffDay(Date d1, Date d2) {
		long between = Math.abs((d2.getTime() - d1.getTime()) / 1000);
		long day = between / 60 / 60 / 24;
		return (long) Math.floor(day);
	}

	/**
	 * 返回传入时间月份的最后一天
	 * */
	public static Date lastDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int value = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, value);
		return cal.getTime();
	}

	/**
	 * 返回传入时间月份的第一天
	 * */
	public static Date firstDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int value = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, value);
		return cal.getTime();
	}

	/**
	 * 获取两个时间相差月份
	 * */
	public static int getDiffMonth(Date start, Date end) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(start);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(end);
		return (endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR)) * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
	}

	/**
	 * 计算并格式化消耗时间<br>
	 * generate by: vakin jiang at 2012-2-16
	 * 
	 * @param startPoint
	 * @return
	 */
	public static String formatTimeConsumingInfo(long startPoint) {
		StringBuffer buff = new StringBuffer();
		long totalMilTimes = System.currentTimeMillis() - startPoint;
		int hour = (int) Math.floor(totalMilTimes / (60 * 60 * 1000));
		int mi = (int) Math.floor(totalMilTimes / (60 * 1000));
		int se = (int) Math.floor((totalMilTimes - 60000 * mi) / 1000);
		if (hour > 0)
			buff.append(hour).append("小时");
		if (mi > 0)
			buff.append(mi).append("分");
		if (hour == 0)
			buff.append(se).append("秒");
		return buff.toString();
	}

	/**
	 * 判断是否为闰年<br>
	 * generate by: zengqw at 2012-9-26
	 */
	public static boolean isLeapYear(int year) {
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}

	public static Date add(Date date, int calendarField, int amount) {
		if (date == null) {
			throw new IllegalArgumentException("The date must not be null");
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(calendarField, amount);
		return c.getTime();
	}
	/**
	 * 获取最近六个月
	 * generate by: CL at 2019-12-27
	 * @return
	 */
	public static ArrayList<String> getSixMonth ()throws ParseException {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -4);
		String before_six = c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH);//六个月前
		ArrayList<String> result = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");// 格式化为年月

		Calendar min = Calendar.getInstance();
		Calendar max = Calendar.getInstance();
		min.setTime(DateUtilLocal.parseDate(before_six,"yyyy-MM"));
		min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);
		max.setTime(DateUtilLocal.parseDate(DateUtilLocal.format(new Date()),"yyyy-MM"));
		max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);
		Calendar curr = min;
		while (curr.before(max)) {
			result.add(DateUtilLocal.format(curr.getTime(),"yyyy-MM"));
			curr.add(Calendar.MONTH, 1);
		}
		//System.out.println(result);
		return result;
	}
	/**
	 * 获取当前季节
	 * generate by: CL at 2019-12-27
	 * @return
	 */
	public  static String getSeason(){
		int seasonNumber = Calendar.getInstance().get(Calendar.MONTH);
		return seasonNumber>=1&&seasonNumber<=3?"春":seasonNumber>=4&&seasonNumber<=6?"夏":seasonNumber>=7&&seasonNumber<=9?"秋":seasonNumber>=10?"冬":"冬";
	}
	/**
	 * 获取时间戳
	 */
	public static String getTimeString() {
		SimpleDateFormat df = new SimpleDateFormat(FORMAT_FULL);
		Calendar calendar = Calendar.getInstance();
		return df.format(calendar.getTime());
	}

	/**
	 * 获取日期年份
	 * @param date 日期
	 * @return
	 */
	public static String getYear(Date date) {
		return format(date).substring(0, 4);
	}
	/**
	 * 功能描述：返回月
	 *
	 * @param date
	 *            Date 日期
	 * @return 返回月份
	 */
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 功能描述：返回日
	 *
	 * @param date
	 *            Date 日期
	 * @return 返回日份
	 */
	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 功能描述：返回小
	 *
	 * @param date
	 *            日期
	 * @return 返回小时
	 */
	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 功能描述：返回分
	 *
	 * @param date
	 *            日期
	 * @return 返回分钟
	 */
	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	/**
	 * 返回秒钟
	 *
	 * @param date
	 *            Date 日期
	 * @return 返回秒钟
	 */
	public static int getSecond(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.SECOND);
	}

	/**
	 * 功能描述：返回毫
	 *
	 * @param date
	 *            日期
	 * @return 返回毫
	 */
	public static long getMillis(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getTimeInMillis();
	}
	/**
	 * 功能描述：返回时间的下一天
	 *
	 * @param date
	 *            日期
	 * @return 返回毫
	 */
	public static Date getNextDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
		date = calendar.getTime();
		return date;
	}
	/**
	 * 功能描述：返回时间的上一天
	 *
	 * @param date
	 *            日期
	 * @return 返回毫
	 */
	public static Date getLastDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);//-1今天的时间减一天
		date = calendar.getTime();
		return date;
	}
	/**
	 * 功能描述：返回时间的相加的日期
	 *
	 * @param days
	 *            日期
	 * @return 返回毫
	 */
	public static  Date getDateAdd(int days){
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -days+1);
		return c.getTime();
	}
	/**
	 * 功能描述：获取最近几天的日期
	 *
	 * @param days
	 *            日期
	 * @return 返回毫
	 */
	public static  List<String> getDaysBetwwen(int days){
		List<String> dayss = new ArrayList<>();
		Calendar start = Calendar.getInstance();
		start.setTime(new Date());
		Long startTIme = start.getTimeInMillis();
		Calendar end = Calendar.getInstance();
		end.setTime(getDateAdd(days));
		Long endTime = end.getTimeInMillis();
		Long oneDay = 1000 * 60 * 60 * 24l;
		Long time = startTIme;
		while (time >= endTime) {
			Date d = new Date(time);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			System.out.println(df.format(d));
			dayss.add(df.format(d));
			time -= oneDay;
		}
		return dayss;
	}
	public static String getTimeByMinutes(Integer totalMinutes) {
		String totalStr="";
		int year = totalMinutes/(24*60*30*12);
		int month = (totalMinutes%(24*60*30*12))/(24*60*30);
		int day = (totalMinutes%(24*60*30))/(24*60);
		int hour = (totalMinutes%(24*60))/60;
		int minute = (totalMinutes%(24*60))%60;
		if(minute>0){
			totalStr=minute+"分";
		}
		if(hour>0){
			totalStr=hour+"小时"+totalStr;
		}
		if(day>0){
			totalStr=day+"天"+totalStr;
		}
		if(month>0){
			totalStr=month+"月"+totalStr;
		}
		if(year>0){
			totalStr=year+"年"+totalStr;
		}
		if(Detect.isEmpty(totalStr)){
			totalStr="0分";
		}
		return totalStr;
	}
	/**
	 * @Description 是否为当天24h内
	 * @author CL
	 * @param inputJudgeDate 要判断是否在当天24h内的时间
	 * @return
	 * boolean
	 */
	public static boolean isToday(Date inputJudgeDate) {
		boolean flag = false;
		if(inputJudgeDate==null){
			return flag;
		}
		//获取当前系统时间
		long longDate = System.currentTimeMillis();
		Date nowDate = new Date(longDate);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = dateFormat.format(nowDate);
		String subDate = format.substring(0, 10);
		//定义每天的24h时间范围
		String beginTime = subDate + " 00:00:00";
		String endTime = subDate + " 23:59:59";
		Date paseBeginTime = null;
		Date paseEndTime = null;
		try {
			paseBeginTime = dateFormat.parse(beginTime);
			paseEndTime = dateFormat.parse(endTime);

		} catch (ParseException e) {
			System.out.println(e.toString());
		}
		if(inputJudgeDate.after(paseBeginTime) && inputJudgeDate.before(paseEndTime)) {
			flag = true;
		}
		return flag;
	}
	public static boolean check(String starttime,String endtime) {
		boolean flag=false;
		if(Detect.isEmpty(starttime)||Detect.isEmpty(endtime)){
			return false;
		}
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");//设置日期格式
		//初始化
		Date nowTime = null;
		Date beginTime = null;
		Date endTime = null;
		try {
			//格式化当前时间格式
			nowTime = df.parse(df.format(new Date()));
			//定义开始时间
			beginTime = df.parse(starttime);
			//定义结束时间
			endTime = df.parse(endtime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//调用判断方法
		flag = belongCalendar(nowTime, beginTime, endTime);
		return flag;
	}

	/**
	 * 判断时间是否在时间段内
	 *
	 * @param nowTime
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	private static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime) {
		//设置当前时间
		Calendar date = Calendar.getInstance();
		date.setTime(nowTime);
		//设置开始时间
		Calendar begin = Calendar.getInstance();
		begin.setTime(beginTime);
		//设置结束时间
		Calendar end = Calendar.getInstance();
		end.setTime(endTime);
		//处于开始时间之后，和结束时间之前的判断
		if (date.after(begin) && date.before(end)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 获取一天中间隔30分钟的所有时间点
	 *
	 * @return
	 */
	public static List<String> getTimeLag30Minute(Integer minutes) {
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		SimpleDateFormat format=new SimpleDateFormat("HH:mm");
		String s = df.format(day);
		Date date = null;
		try {
			date = df.parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int length=1440/minutes;
		List<String> dates = new ArrayList<String>();
		for (int i = 0; i <length; i++) {
			if(i==0){
				dates.add("00:00");
			}else{
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.MINUTE, minutes);
				date = cal.getTime();
				String s1 = format.format(date);
				dates.add(s1);
			}
		}
		return dates;
	}
	public static  String getNowTimeStr()
	{
		Date currentTime = new Date();
		//改变输出格式（自己想要的格式）
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//得到字符串时间
		String s8 = formatter.format(currentTime);
		return s8;
	}
	//根据日期取得星期几
	public static Integer getWeek(Date date){
		String[] weeks = {"7","1","2","3","4","5","6"};
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if(week_index<0){
			week_index = 0;
		}
		return Integer.parseInt(weeks[week_index]);
	}
	public static String dealDateFormat(String oldDateStr)  {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SS");  //yyyy-MM-dd'T'HH:mm:ss
		Date date = null;
		try {
			date = df.parse(oldDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		date.setHours(date.getHours()+8); //  一个 T 是 8 小时，虽然不知道为什么等于 8 ，但可以测试出来
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		return df2.format(date);
	}
	//获取最近几天的数组
	public static List<String> getDateList(Integer days) {
		List<String> dateList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		for (int i = 0; i < days; i++) {
			Date oldDate =  DateUtil.offsetDay(new Date(), -days+1);
			Date date =  DateUtil.offsetDay(oldDate, +i);
			String formatDate = sdf.format(date);
			dateList.add(formatDate);
		}
		return dateList;
	}
	//比较两个字符串日期大小
	public static boolean compareDate(String startTime,String endTime){
		Date startDate = DateUtil.parse(startTime);
		Date endDate = DateUtil.parse(endTime);
		if(startDate.getTime()>=endDate.getTime()){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 获取精确到秒的时间戳
	 * @param date
	 * @return
	 */
	public static int getSecondTimestampTwo(Date date){
		if (null == date) {
			return 0;
		}
		String timestamp = String.valueOf(date.getTime()/1000);
		return Integer.valueOf(timestamp);
	}

	/**
	 * 获取时间间隔（分钟）hutool的DateUtil
	 * @return
	 */
	public static int getBetweenMinute(Date date){
//        DateUtil.beginOfDay(new Date())当天时间凌晨
		//当前时间 	目标时间		时间间隔类型（时、分、秒。。。）
		long between = DateUtil.between(DateUtil.date(), date, DateUnit.MINUTE);
		return Math.toIntExact(between);
	}
	/**
	 * 生成相隔时间的数组
	 */
	public static List<String> getDateListByMinute(String startDateStr,String endDateStr,Integer minute){
		Date startDate=parseDate(startDateStr,TIMESTAMP_PATTERN);
		Date endDate=parseDate(endDateStr,TIMESTAMP_PATTERN);
		List<Date> ds = getDateByMinute(startDate,endDate,minute);
		List<String> list = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Date d : ds) {
			list.add(sdf.format(d));
		}
		return list;
	}
	static List<Date> getDateByMinute(Date startDate,Date endDate,Integer minute) {

		List<Date> result = new ArrayList<Date>();
		while (startDate.compareTo(endDate) <=0) {
			result.add(startDate);
			//日期加5分钟
			startDate = addFiveMin(startDate, minute);
		}
		return result;
	}

	private static Date addFiveMin(Date start, int offset) {
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.add(Calendar.MINUTE, offset);
		return c.getTime();
	}

	private static Date nextDay(Date start) {
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.add(Calendar.DATE, 1);
		return c.getTime();
	}

	private static Date dayStartDate(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public static void main(String[] args) throws Exception{
//		Date dateAdd = getDateAdd(30);
//		System.out.println(DateUtil.format(dateAdd,DateUtil.DATE_PATTERN));
//		List<String> minutesDate = getTimeLag30Minute(15);
//		String format = DateUtil.format(DateUtil.parseDate("2021-06-09 09:00:48"), DateUtil.TIME_PATTERN);
//		String format1 = DateUtil.format(DateUtil.parseDate("2021-06-09 10:15:38"), DateUtil.TIME_PATTERN);
//		Map<String,Double> map=new HashMap<>();
//		map.put(format,0.8d);
//		map.put(format1,1.3d);
//		List<Double> doubleList=new ArrayList<>();
//		for(int i=0;i<minutesDate.size();i++){
//			String s = minutesDate.get(i);
//			Double s1 = map.get(s);
//			if(s1!=null){
//				doubleList.add(s1);
//			}else{
//				doubleList.add(0.0);
//			}
//		}
//		System.out.println(doubleList.size());
//		Date date = DateUtilLocal.parseDate("2021-06-23 23:59:59");
//		System.out.println(date.getTime());
//		System.out.println(new Date().getTime());
//		if(date.getTime()<new Date().getTime()){
//			System.out.println("超期");
//		}else{
//			long diffDay = getDiffDay(new Date(), date);
//			System.out.println(diffDay);
//		}
//		boolean b = compareDate("20220702", "20220702");
//		System.out.println(b);
		//System.out.println(getDateListByMinute("2023-1-29 4:13:55","2023-01-29 10:13:55",10));
		String s = formatDateStr("2023-1-29 4:13:55");
		System.out.println(s);
	}
}
