package com.ruoyi.ghxx.vo;

import lombok.Data;

@Data
public class BDeviceEchartsData {
    private Integer timeNum;
    private Double  avgDataA1;
    private Double  avgDataA2;
    private Double  avgDataA3;
    private Double  avgDataA4;
    private Double  avgDataA5;
    private Double  avgDataA6;
    private Double  avgDataA7;
    private Double  avgDataA8;
    private Double  avgDataA9;
    private Double  avgDataA10;
    private Double  avgDataA11;
    private Double  avgDataA12;
    private Double  avgDataA13;
    private Double  avgDataA14;
    private Double  avgDataA15;
    private Double  avgDataA16;
    private Double  avgDataA17;
    private Double  avgDataA18;
    private Double  avgDataA19;
    private Double  avgDataA20;
    private Double  avgDataA21;
    private Double  avgDataA22;
    private Double  avgDataA23;
    private Double  avgDataA24;;
}
