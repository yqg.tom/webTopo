package com.ruoyi.ghxx.mapper;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ruoyi.ghxx.domain.BEchartZt;

/**
 * 大屏管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-24
 */
@Service
public interface BEchartZtMapper 
{
    /**
     * 查询大屏管理
     * 
     * @param id 大屏管理主键
     * @return 大屏管理
     */
    public BEchartZt selectBEchartZtById(Long id);

    /**
     * 查询大屏管理列表
     * 
     * @param bEchartZt 大屏管理
     * @return 大屏管理集合
     */
    public List<BEchartZt> selectBEchartZtList(BEchartZt bEchartZt);

    /**
     * 新增大屏管理
     * 
     * @param bEchartZt 大屏管理
     * @return 结果
     */
    public int insertBEchartZt(BEchartZt bEchartZt);

    /**
     * 修改大屏管理
     * 
     * @param bEchartZt 大屏管理
     * @return 结果
     */
    public int updateBEchartZt(BEchartZt bEchartZt);

    /**
     * 删除大屏管理
     * 
     * @param id 大屏管理主键
     * @return 结果
     */
    public int deleteBEchartZtById(Long id);

    /**
     * 批量删除大屏管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBEchartZtByIds(Long[] ids);
}
