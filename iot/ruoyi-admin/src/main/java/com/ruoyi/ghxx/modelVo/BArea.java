package com.ruoyi.ghxx.modelVo;

import lombok.Data;

@Data
public class BArea {
    private Long id;
    private String cityEn;
    private String cityZh;
    private String provinceEn;
    private String provinceZh;
    private String countryEn;
    private String countryZh;
    private String leaderEn;
    private String leaderZh;
    private String lat;
    private String lon;
    private Integer lvl;//1=省，2=市，3=区
}
