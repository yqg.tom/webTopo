package com.ruoyi.ghxx.service;

import com.ruoyi.ghxx.domain.BDeviceZt;

import java.util.List;

/**
 * 云组态Service接口
 * 
 * @author ruoyi
 * @date 2022-05-21
 */
public interface IBDeviceZtService 
{
    /**
     * 查询云组态
     * 
     * @param id 云组态ID
     * @return 云组态
     */
    public BDeviceZt selectBDeviceZtById(Long id);

    /**
     * 查询云组态列表
     * 
     * @param bDeviceZt 云组态
     * @return 云组态集合
     */
    public List<BDeviceZt> selectBDeviceZtList(BDeviceZt bDeviceZt);

    /**
     * 新增云组态
     * 
     * @param bDeviceZt 云组态
     * @return 结果
     */
    public int insertBDeviceZt(BDeviceZt bDeviceZt);

    /**
     * 修改云组态
     * 
     * @param bDeviceZt 云组态
     * @return 结果
     */
    public int updateBDeviceZt(BDeviceZt bDeviceZt);

    /**
     * 批量删除云组态
     * 
     * @param ids 需要删除的云组态ID
     * @return 结果
     */
    public int deleteBDeviceZtByIds(Long[] ids);

    /**
     * 删除云组态信息
     * 
     * @param id 云组态ID
     * @return 结果
     */
    public int deleteBDeviceZtById(Long id);

    /*
    *
    * 下载部署压缩包
    * */
    public String download(BDeviceZt bDeviceZt);
}
