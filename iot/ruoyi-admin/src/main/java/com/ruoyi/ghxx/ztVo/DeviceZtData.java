package com.ruoyi.ghxx.ztVo;

import lombok.Data;

import java.util.List;

@Data
public class DeviceZtData {
    private List<Component> components;
}
