package com.ruoyi.ghxx.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 上传信息对象 sys_upload
 *
 * @author ruoyi
 * @date 2020-11-21
 */
@Data
public class SysUpload extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id唯一标识 */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fileName;

    /** 模块名称 */
    @Excel(name = "模块名称")
    private String moduleName;

    /** 模块关联guid */
    @Excel(name = "模块关联guid")
    private String moduleGuid;

    /** 资源请求路径 */
    @Excel(name = "资源请求路径")
    private String resourceUrl;
    /** 版本号 */
    @Excel(name = "版本号")
    private Integer versionCode;

    /** 版本名称、打包标识 */
    @Excel(name = "版本名称")
    private String versionName;
}