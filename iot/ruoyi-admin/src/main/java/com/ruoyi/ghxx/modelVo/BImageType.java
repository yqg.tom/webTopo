package com.ruoyi.ghxx.modelVo;

import lombok.Data;

@Data
public class BImageType {
    private String id;
    private String title;
    private Long number;
}
