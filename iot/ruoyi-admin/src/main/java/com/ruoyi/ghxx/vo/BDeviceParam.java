package com.ruoyi.ghxx.vo;

import lombok.Data;

@Data
public class BDeviceParam {
    //参数名称
    private String paramName;
    //参数值
    private Object paramValue;
    //参数单位
    private String paramUnit;
    //参数类型0模拟量1开关量
    private Integer paramType;
    //参数字段
    private String paramField;
    //是否上限报警
    private Boolean isUpWarn=false;
    //是否下限报警
    private Boolean isLowerWarn=false;
    //是否图形展示
    private Boolean isDisplay;
    //参数比率
    private Double paramRatio;
    //是否状态报警
    private Boolean isStateWarn=false;

    //图标Url
    private String imageUrl;
}
