package com.ruoyi.ghxx.service;

import java.util.List;
import com.ruoyi.ghxx.domain.SysUpload;

/**
 * 上传信息Service接口
 * 
 * @author ruoyi
 * @date 2020-11-17
 */
public interface ISysUploadService 
{
    /**
     * 查询上传信息
     * 
     * @param id 上传信息ID
     * @return 上传信息
     */
    public SysUpload selectSysUploadById(Long id);

    /**
     * 查询上传信息列表
     * 
     * @param sysUpload 上传信息
     * @return 上传信息集合
     */
    public List<SysUpload> selectSysUploadList(SysUpload sysUpload);

    /**
     * 新增上传信息
     * 
     * @param sysUpload 上传信息
     * @return 结果
     */
    public int insertSysUpload(SysUpload sysUpload);

    /**
     * 修改上传信息
     * 
     * @param sysUpload 上传信息
     * @return 结果
     */
    public int updateSysUpload(SysUpload sysUpload);

    /**
     * 批量删除上传信息
     * 
     * @param ids 需要删除的上传信息ID
     * @return 结果
     */
    public int deleteSysUploadByIds(Long[] ids);

    /**
     * 删除上传信息信息
     * 
     * @param id 上传信息ID
     * @return 结果
     */
    public int deleteSysUploadById(Long id);
}
