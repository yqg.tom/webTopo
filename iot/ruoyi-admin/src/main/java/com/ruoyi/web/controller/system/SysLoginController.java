package com.ruoyi.web.controller.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.HttpPostAndGetUtil;
import com.ruoyi.common.vo.WxLogin;
import com.ruoyi.ghxx.util.Detect;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysMenuService;

/**
 * 登录验证
 * 
 * @author ruoyi
 */
@RestController
public class SysLoginController
{
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private RedisCache redisCache;
    /**
     * 登录方法
     * 
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid(),loginBody.isNoCaptcha());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }
    /**
     * 通过二维码获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getUserByCode")
    public AjaxResult getInfo(String codeGuid)
    {
        AjaxResult ajax = AjaxResult.success();
        String captcha = redisCache.getCacheObject(codeGuid);
        LoginBody loginBody = JSON.parseObject(captcha, LoginBody.class);
        if(loginBody==null){
            loginBody=new LoginBody();
            loginBody.setUsername("");
            redisCache.setCacheObject(codeGuid,JSON.toJSONString(loginBody),60,TimeUnit.SECONDS);
            ajax.put("loginBody",loginBody);
        }else{
            ajax.put("loginBody",loginBody);
        }
        return ajax;
    }
    /**
     * 通过二维码获取用户信息
     *
     * @return 用户信息
     */
    @PostMapping("scanCodeLogin")
    public AjaxResult scanCodeLogin(@RequestBody  LoginBody loginUser)
    {
        String captcha = redisCache.getCacheObject(loginUser.getUuid());
        LoginBody loginBody = JSON.parseObject(captcha, LoginBody.class);
        if(loginBody==null){
            loginBody=new LoginBody();
            loginBody.setUsername(loginUser.getUsername());
            redisCache.setCacheObject(loginUser.getUuid(),JSON.toJSONString(loginBody),60,TimeUnit.SECONDS);
        }else{
            loginBody.setUsername(loginUser.getUsername());
            redisCache.setCacheObject(loginUser.getUuid(),JSON.toJSONString(loginBody),60,TimeUnit.SECONDS);
        }
        return AjaxResult.success();
    }
    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        ajax.put("dept", user.getDept());
        return ajax;
    }

    /**
     * 获取路由信息
     * 
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());

        // 用户信息
        SysUser user = loginUser.getUser();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(user.getUserId());
        return AjaxResult.success(menuService.buildMenus(menus));
    }
}
