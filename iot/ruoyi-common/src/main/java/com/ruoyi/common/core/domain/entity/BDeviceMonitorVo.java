package com.ruoyi.common.core.domain.entity;


import java.util.List;


public class BDeviceMonitorVo {

    //设备总数
    private Integer deviceSumNumber;
    //在线总数
    private Integer onLineNumber;
    //掉线总数
    private Integer offLineNumber;
    //温度报警
    private Integer warnNumber;

    public void setDeviceSumNumber(Integer deviceSumNumber) {
        this.deviceSumNumber = deviceSumNumber;
    }

    public void setOnLineNumber(Integer onLineNumber) {
        this.onLineNumber = onLineNumber;
    }

    public void setOffLineNumber(Integer offLineNumber) {
        this.offLineNumber = offLineNumber;
    }

    public void setWarnNumber(Integer warnNumber) {
        this.warnNumber = warnNumber;
    }

    public Integer getDeviceSumNumber() {
        return deviceSumNumber;
    }

    public Integer getOnLineNumber() {
        return onLineNumber;
    }

    public Integer getOffLineNumber() {
        return offLineNumber;
    }

    public Integer getWarnNumber() {
        return warnNumber;
    }
}
